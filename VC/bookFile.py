from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from Model.book import Book
from Model.list import ListObject
from kivy.uix.textinput import TextInput
from UI.UI import *


class BookFile(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller
        ########################
        #     TITLE LAYOUT     #
        ########################
        # MAIN LABEL
        main_label = Label(text="Fiche du livre")
        main_label.pos_hint = {'x': .0, 'y': .42}
        self.add_widget(main_label)
        ########################
        #      BACKGROUND      #
        ########################
        btn_background1 = Button(size_hint_y=.69, size_hint_x=.61, disabled=True)
        btn_background1.pos_hint = {'x': .055, 'y': .195}
        self.add_widget(btn_background1)
        # LABEL BOOK PRICE
        label_price = Label(text="Prix: ",size_hint_y=.08, size_hint_x=.1)
        label_price.pos_hint = {'x': .06, 'y': .8}
        self.add_widget(label_price)
        # BOOK ID
        label_bookid = Button(text="Id du livre :", size_hint_y=.08, size_hint_x=.6, disabled=True)
        label_bookid.pos_hint = {'x': .06, 'y': .8}
        self.add_widget(label_bookid)
        # LABEL BOOK AUTHOR
        label_author = Button(text="Auteur", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_author.pos_hint = {'x': .24, 'y': .72}
        self.add_widget(label_author)
        # LABEL BOOK TITLE
        label_title = Button(text="Titre:", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_title.pos_hint = {'x': .24, 'y': .64}
        self.add_widget(label_title)
        # LABEL BOOK TYPE
        label_type = Button(text="Type:", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_type.pos_hint = {'x': .24, 'y': .56}
        self.add_widget(label_type)
        # LABEL BOOK GENRE
        label_genre = Button(text="Genre:", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_genre.pos_hint = {'x': .24, 'y': .48}
        self.add_widget(label_genre)
        # LABEL BOOK SYNOPSIS
        label_synopsis = Button(text="Résumé:", size_hint_y=.28, size_hint_x=.18, disabled=True)
        label_synopsis.pos_hint = {'x': .24, 'y': .20}
        self.add_widget(label_synopsis)
        # LABEL BOOK BARCODE IMAGE
        image_code = Button(text="Code barre", size_hint_y=.08, size_hint_x=.18, disabled=True)
        image_code.pos_hint = {'x': .06, 'y': .30}
        self.add_widget(image_code)
        # LABEL BOOK COVER IMAGE
        image_cover = Button(text="Couverture", size_hint_y=.08, size_hint_x=.18, disabled=True)
        image_cover.pos_hint = {'x': .06, 'y': .72}
        self.add_widget(image_cover)
        ########################
        #   BOOK DATA LABEL    #
        ########################
        self.book_id = Label(text="", size_hint_y=.08, size_hint_x=.18, pos_hint={'x': .335, 'y': .801})
        self.book_author = Label(text="", size_hint_y=.08, size_hint_x=.18, pos_hint={'x': .42, 'y': .72})
        self.book_name = Label(text="", size_hint_y=.08, size_hint_x=.18, pos_hint={'x': .42, 'y': .64})
        self.book_type = Label(text="", size_hint_y=.08, size_hint_x=.18, pos_hint={'x': .42, 'y': .56})
        self.book_genre = Label(text="", size_hint_y=.08, size_hint_x=.18, pos_hint={'x': .42, 'y': .48})
        self.book_synopsis = TextInput(focus=True, disabled=True, auto_indent=True, text="", size_hint_y=.28, size_hint_x=.24, pos_hint={'x': .42, 'y': .20})
        self.book_price = Label(text="",  size_hint_y=.08, size_hint_x=.06, pos_hint={'x': .12, 'y': .80})
        self.book_cover = Label(text="", size_hint_y=.08, size_hint_x=.18, pos_hint={'x': .1, 'y': .1})
        self.book_barcode = Label(text="", size_hint_y=.08, size_hint_x=.18, pos_hint={'x': .1, 'y': .1})

        ########################
        #      BACKGROUND      #
        ########################
        # BACKGROUND 2
        btn_background2 = Button(size_hint_y=.69, size_hint_x=.292, disabled=True)
        btn_background2.pos_hint = {'x': .674, 'y': .195}
        self.add_widget(btn_background2)
        # LABEL HISTORY BOOK
        btn_history = Button(text="Historique", size_hint_y=.06, size_hint_x=.28, disabled=True)
        btn_history.pos_hint = {'x': .68, 'y': .82}
        self.add_widget(btn_history)
        # BUTTON HISTORY BORROWS
        self.btn_borrow_history = BookToggleButton(text="Emprunt", book=None, size_hint_y=.06, size_hint_x=.14)
        self.btn_borrow_history.pos_hint = {'x': .82, 'y': .76}
        self.btn_borrow_history.bind(on_release=self.charge_borrows_history)
        # BUTTON HISTORY SELLS
        self.btn_sell_history = BookToggleButton(text="Vente", book=None, size_hint_y=.06, size_hint_x=.14, state="down")
        self.btn_sell_history.pos_hint = {'x': .68, 'y': .76}
        self.btn_sell_history.bind(on_release=self.charge_sells_history)
        ########################
        # SCROLL_VIEW LAYOUT   #
        ########################
        # SCROLL_VIEW LAYOUT + WIDGET
        self.scroll_layout = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout.bind(minimum_height=self.scroll_layout.setter('height'))
        # BUTTON IN SCROLL_VIEW
        self.scroll_view = ScrollView(size_hint=(.28, .38), size=(Window.width, Window.height))
        self.scroll_view.pos_hint = {'x': .68, 'y': .38}
        self.scroll_view.add_widget(self.scroll_layout)
        self.add_widget(self.scroll_view)
        self.list_book_button()
        # LABEL RESERVATION
        label_reservation = Button(text="Réservation:", size_hint_y=.06, size_hint_x=.28, disabled=True)
        label_reservation.pos_hint = {'x': .68, 'y': .30}
        self.add_widget(label_reservation)
        ########################
        #        FOOTER        #
        ########################
        # BUTTON RETURN
        btn_return = Button(text="Retour", size_hint_y=.1, size_hint_x=.2)
        btn_return.pos_hint = {'x': .04, 'y': .02}
        btn_return.bind(on_release=self.clbk_return)
        self.add_widget(btn_return)
        # BUTTON BUY BOOK
        btn_buy = Button(text="Acheter", size_hint_y=.1, size_hint_x=.2)
        btn_buy.pos_hint = {'x': .28, 'y': .02}
        #self.btn_buy.bind()
        self.add_widget(btn_buy)
        # BUTTON BORROW BOOK
        btn_borrow = Button(text="Emprunter", size_hint_y=.1, size_hint_x=.2)
        btn_borrow.pos_hint = {'x': .52, 'y': .02}
        #self.btn_borrow.bind()
        self.add_widget(btn_borrow)
        # BUTTON MODIFY BOOK
        btn_modify = Button(text="Modifier", size_hint_y=.1, size_hint_x=.2)
        btn_modify.pos_hint = {'x': .76, 'y': .02}
        btn_modify.bind(on_release=self.clbk_to_modify)
        self.add_widget(btn_modify)
        ########################

        # CHARGE ALL WIDGET ONTO SELF
        self.layout()

    def layout(self):
        self.add_widget(self.book_id)
        self.add_widget(self.book_name)
        self.add_widget(self.book_author)
        self.add_widget(self.book_price)
        self.add_widget(self.book_type)
        self.add_widget(self.book_genre)
        self.add_widget(self.book_synopsis)
        self.add_widget(self.book_cover)
        self.add_widget(self.book_barcode)
        self.add_widget(self.btn_borrow_history)
        self.add_widget(self.btn_sell_history)

    def update_widgets(self):
        self.remove_widget(self.book_id)
        self.remove_widget(self.book_name)
        self.remove_widget(self.book_author)
        self.remove_widget(self.book_price)
        self.remove_widget(self.book_type)
        self.remove_widget(self.book_genre)
        self.remove_widget(self.book_synopsis)
        self.remove_widget(self.book_cover)
        self.remove_widget(self.book_barcode)
        self.remove_widget(self.btn_borrow_history)
        self.remove_widget(self.btn_sell_history)
        self.layout()

    def load_book_data(self, book):
        if book:
            self.book_id.text = book.id
            self.book_name.text = book.name
            self.book_author.text = book.author
            self.book_price.text = book.price+"€"
            self.book_type.text = book.type
            self.book_genre.text = book.category
            self.book_synopsis.text = book.abstract
            self.book_cover.text = ""
            self.book_barcode.text = ""
            self.update_widgets()

    def charge_sells_history(self, widget):
        self.btn_borrow_history.state = "normal"
        self.load_history(self.controller.book_buyers)

    def charge_borrows_history(self, widget):
        self.btn_sell_history.state = "normal"
        self.load_history(self.controller.book_borrowers)

    def load_history(self, list_customers):
        self.scroll_layout.clear_widgets()
        for customer in list_customers:
            btn = Button(text=customer.lastname+" "+customer.firstname, size_hint_y=None, height=30)
            self.scroll_layout.add_widget(btn)

    def list_book_button(self):
        self.scroll_layout.clear_widgets()
        for customer in self.controller.book_buyers:
            btn = Button(text=customer.lastname+" "+customer.firstname, size_hint_y=None, height=30)
            self.scroll_layout.add_widget(btn)

    def clbk_return(self, widget):
        self.controller.menu_return()

    def clbk_to_modify(self, widget):
        self.controller.menu_modify()


class BookFileController:
    def __init__(self, menus):
        self.book_borrowers = []
        self.book_buyers = []
        self.menus = menus
        self.view = BookFile(self)

    def menu_return(self):
        self.menus.display_menu(self.menus.menu_library)

    def menu_modify(self):
        self.menus.display_menu(self.menus.modify_book)

    def charge_data(self, book):
        self.charge_history(book)
        self.view.list_book_button()
        self.view.load_book_data(book)
        self.menus.display_menu(self.menus.book_file)

    def charge_history(self, book):
        borrows_list = ListObject()
        sales_list = ListObject()
        self.book_borrowers = borrows_list.load_borrows_list(book)
        self.book_buyers = sales_list.load_sales_list(book)

