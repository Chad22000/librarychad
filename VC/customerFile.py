from UI.UI import *
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.uix.scrollview import ScrollView
from kivy.uix.popup import Popup
import kivy.utils as utils
from Model.Customer import Customer
from Model.list import ListObject
import json


class CustomerFileView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.customer = ""
        self.customer_id = ""
        self.customer_lastname = ""
        self.customer_firstname = ""
        self.customer_address = ""
        self.customer_zipcode = ""
        self.customer_city = ""
        self.customer_phone = ""
        self.customer_mobile = ""
        self.customer_mail = ""
        self.customer_list_boughts = ""
        self.customer_list_borrows = ""

        self.popup = ""
        self.popup_favorites = ""

        # BACKGROUND MENU
        self.menu_background = BackgroundCustomerManagement(text="", size_hint=(1, 1))

        self.label_popup = Button(text="Client supprimé !", disabled=True, background_color=[0.15, 0.7, 1,
                                                                                             1])
        self.label_popup.size_hint = (0.7, 0.09)
        self.label_popup.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.main_grid_content = FloatLayout(size_hint=(1, 1))

        # customer file background
        self.customer_file_background = Button(text="", disabled=True, background_normal="",
                                               background_color=utils.get_color_from_hex('#000000'))
        self.customer_file_background.size_hint = (0.48, 0.70)
        self.customer_file_background.pos_hint = {'x': 0.016, 'y': 0.28}

        # customer file container
        self.customer_file_content = FloatLayout()
        self.customer_file_content.size_hint = (0.46, 0.67)
        self.customer_file_content.pos_hint = {'x': 0.0255, 'y': 0.285}

        # customer bought/borrows history background
        self.customer_history_background = Button(text="", disabled=True, background_normal="",
                                                  background_color=utils.get_color_from_hex('#000000'))
        self.customer_history_background.size_hint = (0.48, 0.70)
        self.customer_history_background.pos_hint = {'x': 0.504, 'y': 0.28}

        # customer history container
        self.customer_history_content = BoxLayout(orientation='vertical')
        self.customer_history_content.size_hint = (0.46, 0.66)
        self.customer_history_content.pos_hint = {'x': 0.514, 'y': 0.30}

        # customer history buttons container
        self.history_buttons_container = GridLayout(cols=2, size_hint=(1, 0.15))

        # scrollview
        self.customer_history_scrollview = ScrollView(size_hint=(1, 1))
        self.grid_customer_history_list = GridLayout(cols=1, size_hint=(1, 1))

        # customer alert background
        self.customer_alert_background = Button(text="", disabled=True, background_normal="",
                                                background_color=utils.get_color_from_hex('#000000'))
        self.customer_alert_background.size_hint = (0.48, 0.15)
        self.customer_alert_background.pos_hint = {'x': 0.016, 'y': 0.117}

        # self customer alert container
        self.customer_alert_content = BoxLayout(spacing=6)
        self.customer_alert_content.size_hint = (0.46, 0.13)
        self.customer_alert_content.pos_hint = {'x': 0.0255, 'y': 0.1275}

        # customer more info background
        self.customer_more_info_background = Button(text="", disabled=True, background_normal="",
                                                    background_color=utils.get_color_from_hex('#000000'))
        self.customer_more_info_background.size_hint = (0.48, 0.15)
        self.customer_more_info_background.pos_hint = {'x': 0.504, 'y': 0.117}

        # self customer more info container
        self.customer_more_info_content = BoxLayout()
        self.customer_more_info_content.size_hint = (0.46, 0.13)
        self.customer_more_info_content.pos_hint = {'x': 0.514, 'y': 0.1275}

        # return button
        self.button_return = Button(text="RETOUR", size_hint=(0.15, 0.07))
        self.button_return.pos_hint = {'x': 0.834, 'y': 0.025}
        self.button_return.bind(on_release=self.cblk_navigate_to_customer_management)

        self.layout1()

    def layout1(self):
        self.clear_widgets()
        # add widgets to layout
        self.add_widget(self.menu_background)
        self.add_widget(self.customer_file_background)
        self.add_widget(self.customer_file_content)
        self.add_widget(self.customer_history_background)
        self.add_widget(self.customer_history_content)
        # ADD WIDGETS CUSTOMER HISTORY #
        self.customer_history_content.add_widget(self.history_buttons_container)
        self.customer_history_content.add_widget(self.customer_history_scrollview)
        self.customer_history_scrollview.add_widget(self.grid_customer_history_list)
        self.add_widget(self.customer_alert_background)
        self.add_widget(self.customer_alert_content)
        self.add_widget(self.customer_more_info_background)
        self.add_widget(self.customer_more_info_content)
        self.add_widget(self.button_return)



    def display_customer_datas(self, customer):
        self.customer_file_content.clear_widgets()
        self.customer_alert_content.clear_widgets()
        self.customer_more_info_content.clear_widgets()

        if customer:
            self.customer = customer
            self.customer_id = customer.id
            self.customer_lastname = customer.lastname
            self.customer_firstname = customer.firstname
            self.customer_address = customer.address
            self.customer_zipcode = customer.zipcode
            self.customer_city = customer.city
            self.customer_phone = customer.phone
            self.customer_mobile = customer.mobile
            self.customer_mail = customer.mail
            self.customer_list_boughts = self.controller.list_customer_boughts
            self.customer_list_borrows = self.controller.list_customer_borrows

            self.load_customer_file()
            self.load_customer_history()
            self.load_customer_alert()
            self.load_customer_more_info()

    def load_customer_file(self):
        ###### FICHE CLIENT ######
        # Titre fiche client
        label_file_title = MyButton(text="CLIENT ID : " + self.customer_id, size_hint=(1.0, 1.0), halign="left",
                                    valign="middle", padding=(10, 0), disabled=True)
        label_file_title.size_hint = (1, 0.1)
        label_file_title.pos_hint = {'center_x': 0.5, 'center_y': 0.96}
        # Nom
        label_lastname = MyButton(text="Nom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                  padding=(5, 0), disabled=True)
        label_lastname.size_hint = (0.3, 0.1)
        label_lastname.pos_hint = {'center_x': 0.15, 'center_y': 0.86}
        input_lastname = MyButton(text=self.customer_lastname, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                  valign="middle", disabled=True)
        input_lastname.size_hint = (0.7, 0.1)
        input_lastname.pos_hint = {'center_x': 0.65, 'center_y': 0.86}
        # Prénom
        label_firstname = MyButton(text="Prénom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                   padding=(5, 0), disabled=True)
        label_firstname.size_hint = (0.3, 0.1)
        label_firstname.pos_hint = {'center_x': 0.15, 'center_y': 0.76}
        input_firstname = MyButton(text=self.customer_firstname, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                   valign="middle", disabled=True)
        input_firstname.size_hint = (0.7, 0.1)
        input_firstname.pos_hint = {'center_x': 0.65, 'center_y': 0.76}
        # address
        label_address = MyButton(text="Adresse :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                 padding=(5, 0), disabled=True)
        label_address.size_hint = (0.3, 0.1)
        label_address.pos_hint = {'center_x': 0.15, 'center_y': 0.66}
        input_address = MyButton(text=self.customer_address, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                 valign="middle", disabled=True)
        input_address.size_hint = (0.7, 0.1)
        input_address.pos_hint = {'center_x': 0.65, 'center_y': 0.66}
        # zipcode
        label_zipcode = MyButton(text="Code Postal :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                 padding=(5, 0), disabled=True)
        label_zipcode.size_hint = (0.3, 0.1)
        label_zipcode.pos_hint = {'center_x': 0.15, 'center_y': 0.56}
        input_zipcode = MyButton(text=self.customer_zipcode, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                 valign="middle", disabled=True)
        input_zipcode.size_hint = (0.7, 0.1)
        input_zipcode.pos_hint = {'center_x': 0.65, 'center_y': 0.56}
        # city
        label_city = MyButton(text="City :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                              padding=(5, 0), disabled=True)
        label_city.size_hint = (0.3, 0.1)
        label_city.pos_hint = {'center_x': 0.15, 'center_y': 0.46}
        input_city = MyButton(text=self.customer_city, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                              valign="middle", disabled=True)
        input_city.size_hint = (0.7, 0.1)
        input_city.pos_hint = {'center_x': 0.65, 'center_y': 0.46}
        # phone
        label_phone = MyButton(text="Téléphone :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                               padding=(5, 0), disabled=True)
        label_phone.size_hint = (0.3, 0.1)
        label_phone.pos_hint = {'center_x': 0.15, 'center_y': 0.36}
        input_phone = MyButton(text=self.customer_phone, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                               valign="middle", disabled=True)
        input_phone.size_hint = (0.7, 0.1)
        input_phone.pos_hint = {'center_x': 0.65, 'center_y': 0.36}
        # mobile
        label_mobile = MyButton(text="Mobile :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                padding=(5, 0), disabled=True)
        label_mobile.size_hint = (0.3, 0.1)
        label_mobile.pos_hint = {'center_x': 0.15, 'center_y': 0.26}
        input_mobile = MyButton(text=self.customer_mobile, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                valign="middle", disabled=True)
        input_mobile.size_hint = (0.7, 0.1)
        input_mobile.pos_hint = {'center_x': 0.65, 'center_y': 0.26}
        # mail
        label_mail = MyButton(text="Adresse Mail :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                              padding=(5, 0), disabled=True)
        label_mail.size_hint = (0.3, 0.1)
        label_mail.pos_hint = {'center_x': 0.15, 'center_y': 0.16}
        input_mail = MyButton(text=self.customer_mail, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                              valign="middle", disabled=True)
        input_mail.size_hint = (0.7, 0.1)
        input_mail.pos_hint = {'center_x': 0.65, 'center_y': 0.16}
        # modify customer button
        self.button_modify_customer = CustomerButton(text="MODIFIER", size_hint=(0.15, 0.05), customer=self.customer)
        self.button_modify_customer.pos_hint = {'x': 0.18, 'y': 0.297}
        self.button_modify_customer.bind(on_release=self.cblk_modify_customer_file)
        # delete customer button
        self.button_delete_customer = CustomerButton(text="SUPPRIMER", size_hint=(0.15, 0.05), customer=self.customer)
        self.button_delete_customer.pos_hint = {'x': 0.335, 'y': 0.297}
        self.button_delete_customer.bind(on_release=self.cblk_delete_customer)

        # ADD WIDGETS CUSTOMER FILE #
        # add widgets to layout customer_file_content
        self.customer_file_content.add_widget(label_file_title)
        self.customer_file_content.add_widget(label_lastname)
        self.customer_file_content.add_widget(input_lastname)
        self.customer_file_content.add_widget(label_firstname)
        self.customer_file_content.add_widget(input_firstname)
        self.customer_file_content.add_widget(label_address)
        self.customer_file_content.add_widget(input_address)
        self.customer_file_content.add_widget(label_zipcode)
        self.customer_file_content.add_widget(input_zipcode)
        self.customer_file_content.add_widget(label_city)
        self.customer_file_content.add_widget(input_city)
        self.customer_file_content.add_widget(label_phone)
        self.customer_file_content.add_widget(input_phone)
        self.customer_file_content.add_widget(label_mobile)
        self.customer_file_content.add_widget(input_mobile)
        self.customer_file_content.add_widget(label_mail)
        self.customer_file_content.add_widget(input_mail)

        # ADD WIDGETS MAIN LAYOUT #
        self.add_widget(self.button_modify_customer)
        self.add_widget(self.button_delete_customer)

    def load_customer_history(self):
        # load customer history
        transaction_type = "sales"
        '''self.controller.load_customer_history(transaction_type, self.customer_id)'''

        # btn = Button(text="Customer History", disabled=True)
        self.boughts_history_button = ToggleButton(text="ACHATS", size_hint=(1, 1))
        self.boughts_history_button.bind(on_release=self.cblk_charge_boughts_history)
        self.borrows_history_button = ToggleButton(text="EMPRUNTS", size_hint=(1, 1))
        self.borrows_history_button.bind(on_release=self.cblk_charge_borrows_history)

        self.history_buttons_container.add_widget(self.boughts_history_button)
        self.history_buttons_container.add_widget(self.borrows_history_button)

        for item in self.controller.list_customer_boughts:
            button_datas_text = item.name
            button_datas = Button(text=button_datas_text, size_hint=(1, None), height=40)

            self.grid_customer_history_list.add_widget(button_datas)


    def cblk_charge_boughts_history(self, widget):
        self.borrows_history_button.state = "normal"
        self.load_history(self.controller.list_customer_boughts)

    def cblk_charge_borrows_history(self, widget):
        self.boughts_history_button.state = "normal"
        self.load_history(self.controller.list_customer_borrows)

    def load_history(self, book_list):
        self.grid_customer_history_list.clear_widgets()
        for item in book_list:
            button_datas_text = item.name
            button_datas = Button(text=button_datas_text, size_hint=(1, None), height=40)

            self.grid_customer_history_list.add_widget(button_datas)

    def load_customer_alert(self):
        self.customer_alert_content.clear_widgets()
        if self.controller.list_customer_alerts:

            btn = Button(text="", background_normal="./Public/images/notificationalert.png",
                         background_down="./Public/images/notificationalertdown.png")
            btn.size_hint = (0.3, 1)
            btn.size_hint_max_x = 150
            btn.bind(on_release=self.cblk_display_alerts)
            label_btn = Button(text='Attention Livres en retard !', disabled=True)

            self.customer_alert_content.add_widget(btn)
            self.customer_alert_content.add_widget(label_btn)

            popup_content = BoxLayout(orientation="vertical")
            self.popup = Popup(title='Alerte enlèvement !', content=popup_content, size_hint=(0.5, 0.5))

            for alert in self.controller.list_customer_alerts:
                alert_text = MyButton(text="Livres en retard depuis le "+alert.end_date+" :", disabled=True,
                                      halign="left", valign="center", padding_x=10, background_color=utils.get_color_from_hex('#2d8fe0'),
                                      height=20, size_hint=(1, 1), size_hint_max_y=30)
                popup_content.add_widget(alert_text)
                for book in alert.list_books:
                    alert_book_name = MyLabel(text=book.id+' - '+book.name+' - '+book.author, halign="left", valign="center", disabled=True,
                                              padding_x=30, size_hint=(1, 1), size_hint_max_y=30)
                    popup_content.add_widget(alert_book_name)

            wrapper = Label()
            popup_content.add_widget(wrapper)
        else:
            btn = Button(text="Rien à signaler", disabled=True)
            self.customer_alert_content.add_widget(btn)

    def cblk_display_alerts(self, widget):
        self.popup.open()

    def load_customer_more_info(self):
        if self.controller.list_customer_favorites:
            customer_favorite_author = self.controller.list_customer_favorites['author']
            customer_favorite_category = self.controller.list_customer_favorites['category']
            customer_favorite_type = self.controller.list_customer_favorites['type']

            btn = Button(text="", background_normal="./Public/images/favorite.png",
                         background_down="./Public/images/favoritedown.png")
            btn.size_hint = (0.3, 1)
            btn.size_hint_max_x = 150
            btn.bind(on_release=self.cblk_display_favorites)

            label_btn = Button(text='Afficher les favoris client', disabled=True)

            self.customer_more_info_content.add_widget(btn)
            self.customer_more_info_content.add_widget(label_btn)

            popup_content = BoxLayout(orientation="vertical")
            self.popup_favorites = Popup(title='Favoris client', content=popup_content, size_hint=(0.5, 0.3),
                                         size_hint_max_y=200, size_hint_max_x=500)

            label_author = MyLabel(text='Auteur : ' + customer_favorite_author, disabled=True, halign='left',
                                   valign='center',
                                   padding_x=10)
            label_category = MyLabel(text='Catégorie : ' + customer_favorite_category, disabled=True, halign='left',
                                     valign='center', padding_x=10)
            label_type = MyLabel(text='Type : ' + customer_favorite_type, disabled=True, halign='left',
                                 valign='center', padding_x=10)

            popup_content.add_widget(label_author)
            popup_content.add_widget(label_category)
            popup_content.add_widget(label_type)

            wrapper = Label()
            popup_content.add_widget(wrapper)

    def cblk_display_favorites(self, widget):
        self.popup_favorites.open()

    def cblk_modify_customer_file(self, widget):
        self.controller.navigate_to_modify_customer_file(widget)

    def cblk_delete_customer(self, widget):
        self.controller.delete_customer(widget)

    def cblk_navigate_to_add_customer_file(self, widget):
        self.controller.navigate_to_add_customer_file()

    def cblk_navigate_to_customer_management(self, widget):
        self.controller.navigate_to_customer_management()


class CustomerFileController:
    def __init__(self, menus):
        self.menus = menus
        self.list_customer_transactions = []
        self.list_customer_boughts = []
        self.list_customer_borrows = []
        self.list_customer_alerts = []
        self.list_late_books = []
        self.list_customer_favorites = []
        self.view = CustomerFileView(self)
        self.customer = ""

    def navigate_to_customer_management(self):
        self.view.grid_customer_history_list.clear_widgets()
        self.view.history_buttons_container.clear_widgets()
        self.view.customer_alert_content.clear_widgets()
        self.view.load_customer_alert()
        self.menus.display_menu(self.menus.menu_customer_management)

    def navigate_to_modify_customer_file(self, widget):
        customer = widget.customer
        self.menus.file_customer_modify.preload_customer(customer)
        self.menus.display_menu(self.menus.file_customer_modify)

    def delete_customer(self, widget):
        customer_id = widget.customer.id
        path = './Datas/customers.json'

        # prepare data for saving
        data = Customer.delete_customer(self, path, customer_id)

        # saving as json
        try:
            self.save_as_json(path, data)
            self.display_pop_up_confirmation()
        except:
            print('error')

    def display_pop_up_confirmation(self):
        self.view.add_widget(self.view.label_popup)
        self.view.customer_file_content.clear_widgets()
        self.view.customer_history_content.clear_widgets()
        self.view.customer_alert_content.clear_widgets()
        self.view.customer_more_info_content.clear_widgets()
        self.view.remove_widget(self.view.button_delete_customer)
        self.view.remove_widget(self.view.button_modify_customer)
        Clock.schedule_once(self.cblk_reload_form, 2)

    def cblk_reload_form(self, dt):
        self.view.remove_widget(self.view.button_modify_customer)
        self.view.remove_widget(self.view.button_delete_customer)
        self.view.remove_widget(self.view.label_popup)
        self.menus.menu_customer_management.preload_list_customer()
        self.menus.display_menu(self.menus.menu_customer_management)

    def save_as_json(self, path, data):
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)

    def preload_customer(self, customer):
        list_bought = ListObject()
        list_borrows = ListObject()
        list_alerts = ListObject()
        # list_late_books = ListObject()
        list_customer_favorites = ListObject()

        self.list_customer_boughts = list_bought.load_list_customer_transactions("sales", customer.id)
        self.list_customer_borrows = list_borrows.load_list_customer_transactions("borrows", customer.id)
        self.list_customer_alerts = list_alerts.list_borrows(customer.id)
        # self.list_late_books = list_late_books.get_customer_late_books(customer.id)
        self.list_customer_favorites = list_customer_favorites.get_customers_favorites(customer.id)
        self.view.display_customer_datas(customer)

    '''def load_customer_history(self, transaction_type, customer_id):
        self.list_customer_transactions = Customer.transactions_history(self, transaction_type, customer_id)'''


