from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.dropdown import DropDown
import kivy.utils as utils
from kivy.uix.label import Label
from UI.UI import MyLabel
from UI.UI import MyButton
from UI.UI import BackgroundRoleManagement
from Model.Employee import Employee
import json
from kivy.clock import Clock


class AddEmployeeFileView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.label_lastname = ""
        self.input_lastname = ""
        self.label_firstname = ""
        self.input_firstname = ""
        self.label_mail = ""
        self.input_mail = ""
        self.label_role = ""
        self.dropdown = ""
        self.mainbutton = ""
        self.label_password = ""
        self.input_password = ""
        self.label_confirm_password = ""
        self.input_confirm_password = ""

        # BACKGROUND MENU
        self.menu_background = BackgroundRoleManagement(text="", size_hint=(1, 1))

        # BACKGROUND MAIN GRID
        self.main_grid_background = Button(disabled=True, size_hint=(0.65, 0.63),
                                           pos_hint={'center_x': 0.5, 'center_y': 0.54}, background_normal="",
                                           background_color=utils.get_color_from_hex('#000000'))

        self.label_popup = Button(text="Employé enregistré avec succès !", disabled=True, background_color=[0.15, 0.7,
                                                                                                            1, 1])
        self.main_grid = GridLayout(rows=3)
        self.main_grid.size_hint = (0.65, 0.90)
        self.main_grid.pos_hint = {'center_x': 0.5, 'center_y': 0.4}

        self.main_grid_content = FloatLayout()
        self.label_file_title = MyButton(text="AJOUTER UN EMPLOYE :", size_hint=(1.0, 1.0), halign="left",
                                         valign="middle", padding=(10, 0), disabled=True)
        # self.label_file_title.font_size = self.label_file_title.width/1
        self.label_file_title.size_hint = (1, 0.1)
        self.label_file_title.pos_hint = {'center_x': 0.5, 'center_y': 0.96}

        self.button_validate = Button(text="ENREGISTRER", size_hint=(0.15, 0.07))
        self.button_validate.pos_hint = {'x': 0.835, 'y': 0.05}
        self.button_validate.bind(on_release=self.cblk_add_employee_to_register)
        self.button_return = Button(text="ANNULER", size_hint=(0.15, 0.07))
        self.button_return.pos_hint = {'x': 0.011, 'y': 0.05}
        self.button_return.bind(on_release=self.cblk_navigate_to_menu_employee_management)

        self.layout1()

    def layout1(self):
        self.clear_widgets()
        self.add_widget(self.menu_background)
        self.add_widget(self.main_grid_background)
        self.add_widget(self.main_grid)
        self.main_grid.add_widget(self.main_grid_content)

        self.add_widget(self.button_validate)
        self.add_widget(self.button_return)

    def preload_form(self):
        self.main_grid_content.clear_widgets()

        self.label_popup.size_hint = (0.7, 0.09)
        self.label_popup.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.label_lastname = MyButton(text="Nom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
        self.label_lastname.size_hint = (0.3, 0.1)
        self.label_lastname.pos_hint = {'center_x': 0.15, 'center_y': 0.86}
        self.input_lastname = TextInput(text="", multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_lastname.size_hint = (0.7, 0.1)
        self.input_lastname.pos_hint = {'center_x': 0.65, 'center_y': 0.86}
        self.label_firstname = MyButton(text="Prénom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                        padding=(10, 0), disabled=True)
        self.label_firstname.size_hint = (0.3, 0.1)
        self.label_firstname.pos_hint = {'center_x': 0.15, 'center_y': 0.76}
        self.input_firstname = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_firstname.size_hint = (0.7, 0.1)
        self.input_firstname.pos_hint = {'center_x': 0.65, 'center_y': 0.76}
        self.label_mail = MyButton(text="Adresse Mail :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                   padding=(10, 0), disabled=True)
        self.label_mail.size_hint = (0.3, 0.1)
        self.label_mail.pos_hint = {'center_x': 0.15, 'center_y': 0.66}
        self.input_mail = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_mail.size_hint = (0.7, 0.1)
        self.input_mail.pos_hint = {'center_x': 0.65, 'center_y': 0.66}

        self.label_role = MyButton(text="Rôle :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                   padding=(10, 0), disabled=True)
        self.label_role.size_hint = (0.3, 0.1)
        self.label_role.pos_hint = {'center_x': 0.15, 'center_y': 0.56}

        self.generate_dropdown()

        self.label_password = MyButton(text="Mot de passe :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
        self.label_password.size_hint = (0.3, 0.1)
        self.label_password.pos_hint = {'center_x': 0.15, 'center_y': 0.46}

        self.input_password = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10), password=True)
        self.input_password.size_hint = (0.7, 0.1)
        self.input_password.pos_hint = {'center_x': 0.65, 'center_y': 0.46}

        self.label_confirm_password = MyButton(text="Confirmer Mot de pass :", size_hint=(1.0, 1.0), halign="right",
                                               valign="middle", padding=(10, 0), disabled=True)
        self.label_confirm_password.size_hint = (0.3, 0.1)
        self.label_confirm_password.pos_hint = {'center_x': 0.15, 'center_y': 0.36}

        self.input_confirm_password = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10), password=True)
        self.input_confirm_password.size_hint = (0.7, 0.1)
        self.input_confirm_password.pos_hint = {'center_x': 0.65, 'center_y': 0.36}

        self.main_grid_content.add_widget(self.label_file_title)
        self.main_grid_content.add_widget(self.label_lastname)
        self.main_grid_content.add_widget(self.input_lastname)
        self.main_grid_content.add_widget(self.label_firstname)
        self.main_grid_content.add_widget(self.input_firstname)
        self.main_grid_content.add_widget(self.label_mail)
        self.main_grid_content.add_widget(self.input_mail)
        self.main_grid_content.add_widget(self.label_password)
        self.main_grid_content.add_widget(self.input_password)
        self.main_grid_content.add_widget(self.label_role)
        self.main_grid_content.add_widget(self.mainbutton)
        self.main_grid_content.add_widget(self.label_confirm_password)
        self.main_grid_content.add_widget(self.input_confirm_password)

    def generate_dropdown(self):
        # create a dropdown with 10 buttons
        liste_role = ["manager", "vendeur"]
        self.dropdown = DropDown()
        for item in liste_role:
            # When adding widgets, we need to specify the height manually
            # (disabling the size_hint_y) so the dropdown can calculate
            # the area it needs.

            btn = Button(text=item, size_hint_y=None, height=50)

            # for each button, attach a callback that will call the select() method
            # on the dropdown. We'll pass the text of the button as the data of the
            # selection.
            btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))

            # then add the button inside the dropdown
            self.dropdown.add_widget(btn)

        # create a big main button
        self.mainbutton = Button(text='Choisir', size_hint=(1.0, 1.0))
        self.mainbutton.size_hint = (0.7, 0.1)
        self.mainbutton.pos_hint = {'center_x': 0.65, 'center_y': 0.56}

        # show the dropdown menu when the main button is released
        # note: all the bind() calls pass the instance of the caller (here, the
        # mainbutton instance) as the first argument of the callback (here,
        # dropdown.open.).
        self.mainbutton.bind(on_release=self.dropdown.open)

        # one last thing, listen for the selection in the dropdown list and
        # assign the data to the button text.
        self.dropdown.bind(on_select=lambda instance, x: setattr(self.mainbutton, 'text', x))

    def set_text_to_fit(self, widget, text):
        pass
        '''widget.text = text
        # for long names, reduce font size until it fits in its widget
        m = self.defaultTextHeightMultiplier
        widget.font_size = widget.height * m
        widget.texture_update()
        while m > 0.3 and widget.texture_size[0] > widget.width:
            m = m - 0.05
            widget.font_size = widget.height * m
            widget.texture_update()'''

    def cblk_add_employee_to_register(self, widget):
        self.controller.add_employee_to_register()

    def cblk_navigate_to_menu_employee_management(self, widget):
        self.controller.navigate_to_menu_employee_management()


class AddEmployeeFileController:
    def __init__(self, menus):
        self.menus = menus
        self.view = AddEmployeeFileView(self)

    def navigate_to_menu_employee_management(self):
        self.menus.display_menu(self.menus.menu_role_management)

    def add_employee_to_register(self):
        # path
        path = './Datas/employees.json'

        # get new_employee datas from add employee form
        employee_id = self.set_employee_id(path)  # set employee id
        lastname = self.view.input_lastname.text
        firstname = self.view.input_firstname.text
        mail = self.view.input_mail.text
        role = self.get_employee_role()
        password = self.view.input_confirm_password.text

        # send data for saving
        try:
            Employee.add_employee_to_register(self, path, employee_id, lastname, firstname, mail, role, password)
            self.display_pop_up_confirmation()
        except:
            pass

    def display_pop_up_confirmation(self):
        self.view.add_widget(self.view.label_popup)
        self.view.main_grid_content.clear_widgets()
        Clock.schedule_once(self.cblk_reload_form, 2)

    def cblk_reload_form(self, dt):
        self.view.remove_widget(self.view.label_popup)
        self.menus.menu_role_management.view.grid_employee_list.clear_widgets()
        self.menus.menu_role_management.view.grid_employee_list_scrollview.clear_widgets()
        self.menus.menu_role_management.preload_list_employees()
        self.menus.display_menu(self.menus.menu_role_management)

    def set_employee_id(self, path):
        user_id = Employee.set_employee_id(self, path)

        return user_id

    def get_employee_role(self):
        role = Employee.set_role(self, self.view.mainbutton.text)
        return role



