from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.clock import Clock
from datetime import date
from datetime import datetime
from Model.Session import Session


class Header(GridLayout):
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, cols=6, rows=1, **kwargs)
        self.controller = controller
        self.session = None
        self.session_message = "Se connecter"
        self.date = self.controller.get_date()
        self.time = self.controller.get_time()

        Clock.schedule_interval(self.cblk_update_clock, 1)
        self.label_date = Label(text=str(self.date), size_hint_y=1, size_hint_x=2)
        self.label_time = Label(text=str(self.time), size_hint_y=1, size_hint_x=2)
        self.button_menu = Button(text="Menu", size_hint_y=1, size_hint_x=1)
        self.button_menu.bind(on_release=self.clbk_load_main_menu)
        self.label_session = Label(text=self.session_message, size_hint_y=1, size_hint_x=1)
        self.button_logout = Button(text="Se déconnecter", size_hint_y=1, size_hint_x=1)
        self.button_logout.bind(on_release=self.cblk_logout)
        self.size_hint_y = 0.07

        self.layout()

    def layout(self):
        self.add_widget(self.label_date)
        self.add_widget(self.label_time)
        self.clbk_session()

    def clbk_session(self):
        if self.controller.session:
            self.session = self.controller.session
            self.remove_widget(self.label_session)
            self.remove_widget(self.button_logout)
            self.add_widget(self.button_menu)
            self.session_message = self.controller.session.username
            self.label_session = Label(text=self.session_message, size_hint_y=1, size_hint_x=1)
            self.button_logout = Button(text="Se déconnecter", size_hint_y=1, size_hint_x=1)
            self.button_logout.bind(on_release=self.cblk_logout)
            self.add_widget(self.label_session)
            self.add_widget(self.button_logout)
        else:
            self.remove_widget(self.label_session)
            self.remove_widget(self.button_logout)
            self.session_message = "Se connecter"
            self.label_session = Label(text=self.session_message, size_hint_y=1, size_hint_x=1)
            self.button_logout = Button(text="Se déconnecter", size_hint_y=1, size_hint_x=1)
            self.add_widget(self.label_session)
            self.add_widget(self.button_logout)
        
    def clbk_load_main_menu(self, widget):
        self.controller.load_main_menu()
        
    def cblk_logout(self, widget):
        self.controller.logout()
        
    def cblk_update_clock(self, dt):
        self.controller.update_clock()


class HeaderController:
    def __init__(self, menus):
        self.menus = menus
        self.session = None
        self.view = Header(self)

    def get_date(self):
        today = date.today()
        return today.strftime("%d %B %Y")

    def get_time(self):
        now = datetime.now()
        return now.strftime("%H:%M:%S")
    
    def update_clock(self):
        now = datetime.now()
        self.view.label_time.text = now.strftime("%H:%M:%S")

    def login(self, employee):
        self.session = Session(employee.id, employee.username, employee.role)
        self.view.clbk_session()

    def load_main_menu(self):
        self.menus.display_menu(self.menus.menu_main)
        
    def logout(self):
        self.session = ""
        self.view.remove_widget(self.view.button_menu)
        self.view.remove_widget(self.view.label_session)
        self.view.clbk_session()
        self.menus.menu_connection.view.input_id.text = ""
        self.menus.menu_connection.view.input_password.text = ""
        self.menus.display_menu(self.menus.menu_connection)
