from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
import kivy.utils as utils
from kivy.uix.label import Label
from UI.UI import MyLabel
from UI.UI import MyButton
from UI.UI import BackgroundCustomerManagement
from Model.Customer import Customer
import json
from Model.list import ListObject


class AddCustomerFileView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.label_lastname = ""
        self.input_lastname = ""
        self.label_firstname = ""
        self.input_firstname = ""
        self.label_address = ""
        self.input_address = ""
        self.label_zipcode = ""
        self.input_zipcode = ""
        self.label_city = ""
        self.input_city = ""
        self.label_phone = ""
        self.input_phone = ""
        self.label_mobile = ""
        self.input_mobile = ""
        self.label_mail = ""
        self.input_mail = ""

        # BACKGROUND MENU
        self.menu_background = BackgroundCustomerManagement(text="", size_hint=(1, 1))

        # BACKGROUND MAIN GRID
        self.main_grid_background = Button(disabled=True, size_hint=(0.65, 0.81),
                                           pos_hint={'center_x': 0.5, 'center_y': 0.55}, background_normal="",
                                           background_color=utils.get_color_from_hex('#000000'))

        self.label_popup = Button(text="Client enregistré avec succès !", disabled=True, background_color=[0.15, 0.7, 1,
                                                                                                           1])
        self.main_grid = GridLayout(rows=3)
        self.main_grid.size_hint = (0.65, 0.90)
        self.main_grid.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.main_grid_content = FloatLayout()
        self.label_file_title = MyButton(text="AJOUTER UN CLIENT :", size_hint=(1.0, 1.0), halign="left",
                                         valign="middle", padding=(10, 0), disabled=True)
        # self.label_file_title.font_size = self.label_file_title.width/1
        self.label_file_title.size_hint = (1, 0.1)
        self.label_file_title.pos_hint = {'center_x': 0.5, 'center_y': 0.96}

        self.button_validate = Button(text="ENREGISTRER", size_hint=(0.15, 0.07))
        self.button_validate.pos_hint = {'x': 0.835, 'y': 0.05}
        self.button_validate.bind(on_release=self.cblk_add_customer_to_register)
        self.button_return = Button(text="ANNULER", size_hint=(0.15, 0.07))
        self.button_return.pos_hint = {'x': 0.011, 'y': 0.05}
        self.button_return.bind(on_release=self.cblk_navigate_to_menu_customer_management)

        self.layout1()

    def layout1(self):
        self.clear_widgets()
        self.add_widget(self.menu_background)
        self.add_widget(self.main_grid_background)
        self.add_widget(self.main_grid)
        self.main_grid.add_widget(self.main_grid_content)

        self.add_widget(self.button_validate)
        self.add_widget(self.button_return)

    def preload_form(self):
        self.main_grid_content.clear_widgets()

        self.label_popup.size_hint = (0.7, 0.09)
        self.label_popup.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.label_lastname = MyButton(text="Nom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
        self.label_lastname.size_hint = (0.3, 0.1)
        self.label_lastname.pos_hint = {'center_x': 0.15, 'center_y': 0.86}
        self.input_lastname = TextInput(text="", multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_lastname.size_hint = (0.7, 0.1)
        self.input_lastname.pos_hint = {'center_x': 0.65, 'center_y': 0.86}
        self.label_firstname = MyButton(text="Prénom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                        padding=(10, 0), disabled=True)
        self.label_firstname.size_hint = (0.3, 0.1)
        self.label_firstname.pos_hint = {'center_x': 0.15, 'center_y': 0.76}
        self.input_firstname = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_firstname.size_hint = (0.7, 0.1)
        self.input_firstname.pos_hint = {'center_x': 0.65, 'center_y': 0.76}
        self.label_address = MyButton(text="Adresse :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                      padding=(10, 0), disabled=True)
        self.label_address.size_hint = (0.3, 0.1)
        self.label_address.pos_hint = {'center_x': 0.15, 'center_y': 0.66}
        self.input_address = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_address.size_hint = (0.7, 0.1)
        self.input_address.pos_hint = {'center_x': 0.65, 'center_y': 0.66}

        self.label_zipcode = MyButton(text="Code Postal :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                      padding=(10, 0), disabled=True)
        self.label_zipcode.size_hint = (0.3, 0.1)
        self.label_zipcode.pos_hint = {'center_x': 0.15, 'center_y': 0.56}
        self.input_zipcode = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_zipcode.size_hint = (0.7, 0.1)
        self.input_zipcode.pos_hint = {'center_x': 0.65, 'center_y': 0.56}
        self.label_city = MyButton(text="City :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                   padding=(10, 0), disabled=True)
        self.label_city.size_hint = (0.3, 0.1)
        self.label_city.pos_hint = {'center_x': 0.15, 'center_y': 0.46}
        self.input_city = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_city.size_hint = (0.7, 0.1)
        self.input_city.pos_hint = {'center_x': 0.65, 'center_y': 0.46}
        self.label_phone = MyButton(text="Téléphone :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                    padding=(10, 0), disabled=True)
        self.label_phone.size_hint = (0.3, 0.1)
        self.label_phone.pos_hint = {'center_x': 0.15, 'center_y': 0.36}
        self.input_phone = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_phone.size_hint = (0.7, 0.1)
        self.input_phone.pos_hint = {'center_x': 0.65, 'center_y': 0.36}
        self.label_mobile = MyButton(text="Mobile :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                     padding=(10, 0), disabled=True)
        self.label_mobile.size_hint = (0.3, 0.1)
        self.label_mobile.pos_hint = {'center_x': 0.15, 'center_y': 0.26}
        self.input_mobile = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_mobile.size_hint = (0.7, 0.1)
        self.input_mobile.pos_hint = {'center_x': 0.65, 'center_y': 0.26}

        self.label_mail = MyButton(text="Adresse Mail :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                   padding=(10, 0), disabled=True)
        self.label_mail.size_hint = (0.3, 0.1)
        self.label_mail.pos_hint = {'center_x': 0.15, 'center_y': 0.16}
        self.input_mail = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
        self.input_mail.size_hint = (0.7, 0.1)
        self.input_mail.pos_hint = {'center_x': 0.65, 'center_y': 0.16}

        self.main_grid_content.add_widget(self.label_file_title)
        self.main_grid_content.add_widget(self.label_lastname)
        self.main_grid_content.add_widget(self.input_lastname)
        self.main_grid_content.add_widget(self.label_firstname)
        self.main_grid_content.add_widget(self.input_firstname)
        self.main_grid_content.add_widget(self.label_address)
        self.main_grid_content.add_widget(self.input_address)
        self.main_grid_content.add_widget(self.label_zipcode)
        self.main_grid_content.add_widget(self.input_zipcode)
        self.main_grid_content.add_widget(self.label_city)
        self.main_grid_content.add_widget(self.input_city)
        self.main_grid_content.add_widget(self.label_phone)
        self.main_grid_content.add_widget(self.input_phone)
        self.main_grid_content.add_widget(self.label_mobile)
        self.main_grid_content.add_widget(self.input_mobile)
        self.main_grid_content.add_widget(self.label_mail)
        self.main_grid_content.add_widget(self.input_mail)

    def set_text_to_fit(self, widget, text):
        pass
        '''widget.text = text
        # for long names, reduce font size until it fits in its widget
        m = self.defaultTextHeightMultiplier
        widget.font_size = widget.height * m
        widget.texture_update()
        while m > 0.3 and widget.texture_size[0] > widget.width:
            m = m - 0.05
            widget.font_size = widget.height * m
            widget.texture_update()'''

    def cblk_add_customer_to_register(self, widget):
        self.controller.add_customer_to_register()

    def cblk_navigate_to_menu_customer_management(self, widget):
        self.controller.navigate_to_menu_customer_management()


class AddCustomerFileController:
    def __init__(self, menus):
        self.menus = menus
        self.view = AddCustomerFileView(self)

    def navigate_to_menu_customer_management(self):
        self.menus.display_menu(self.menus.menu_customer_management)

    def add_customer_to_register(self):
        # path
        path = './Datas/customers.json'

        # get new_employee datas from add employee form
        customer_id = self.set_customer_id(path)  # set employee id
        lastname = self.view.input_lastname.text
        firstname = self.view.input_firstname.text
        address = self.view.input_address.text
        zip_code = self.view.input_zipcode.text
        city = self.view.input_city.text
        phone = self.view.input_phone.text
        mobile = self.view.input_mobile.text
        mail = self.view.input_mail.text

        # prepare data for saving
        data = Customer.save_customers(self, path, customer_id, lastname, firstname, address, zip_code, city, phone,
                                       mobile, mail)

        # saving as json
        try:
            self.save_as_json(path, data)
            self.display_pop_up_confirmation()
        except:
            pass

    def display_pop_up_confirmation(self):
        self.view.add_widget(self.view.label_popup)
        self.view.main_grid_content.clear_widgets()
        Clock.schedule_once(self.cblk_reload_form, 2)

    def cblk_reload_form(self, dt):
        self.view.remove_widget(self.view.label_popup)
        self.menus.menu_customer_management.view.grid_customer_list.clear_widgets()
        self.menus.menu_customer_management.view.grid_customer_list_scrollview.clear_widgets()
        self.menus.menu_customer_management.preload_list_customer()
        self.menus.display_menu(self.menus.menu_customer_management)

    def set_customer_id(self, path):
        file_object = open(path, "r")
        json_content = file_object.read()
        users_list = json.loads(json_content)
        nb_users = len(users_list['customers'])
        user_id = str(nb_users + 1)
        file_object.close()

        return user_id

    def save_as_json(self, path, data):
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)
