from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.textinput import TextInput
from UI.UI import PageIDButton


class MenuGraphic(FloatLayout):
    def __init__(self, controller, **kwargs):
        super().__init__()
        self.controller = controller
        # RETURN BUTTON
        self.buttonreturn = Button(text='Retour', size_hint=(0.15, 0.07))
        self.buttonreturn.pos_hint = {'x': 0.038, 'y': 0.05}
        self.buttonreturn.bind(on_release=self.clbk_previous)


    def layout(self):
        self.clear_widgets()

    def clbk_previous(self, widget):
        self.controller.previous()


class MenuGraphicController:
    def __init__(self, menus):
        self.menus = menus
        self.view = MenuGraphic(self)

    def previous(self):
        self.menus.display_menu(self.menus.menu_main)
