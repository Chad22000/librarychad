from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.clock import Clock
import kivy.utils as utils
from kivy.uix.dropdown import DropDown
from UI.UI import MyButton
from UI.UI import EmployeeButton
from UI.UI import BackgroundRoleManagement
from Model.Employee import Employee
import json


class EmployeeFileView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        # BACKGROUND MENU
        self.menu_background = BackgroundRoleManagement(text="", size_hint=(1, 1))

        # BACKGROUND MAIN GRID
        self.main_grid_background = Button(disabled=True, size_hint=(0.65, 0.35),
                                           pos_hint={'center_x': 0.5, 'center_y': 0.53}, background_normal="",
                                           background_color=utils.get_color_from_hex('#000000'))

        self.label_popup = Button(text="Employé supprimé !", disabled=True, background_color=[0.15, 0.7, 1,
                                                                                                           1])
        self.label_popup.size_hint = (0.7, 0.09)
        self.label_popup.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.main_grid = GridLayout(rows=3)
        self.main_grid.size_hint = (0.65, 0.7)
        self.main_grid.pos_hint = {'center_x': 0.5, 'center_y': 0.35}

        self.main_grid_content = FloatLayout()

        self.button_return = Button(text="ANNULER", size_hint=(0.15, 0.07))
        self.button_return.pos_hint = {'x': 0.011, 'y': 0.05}
        self.button_return.bind(on_release=self.cblk_navigate_to_add_employee_file)

        self.layout1()

    def layout1(self):
        self.clear_widgets()
        self.add_widget(self.menu_background)
        self.add_widget(self.main_grid_background)
        self.add_widget(self.main_grid)
        self.main_grid.add_widget(self.main_grid_content)

        self.add_widget(self.button_return)

    def display_employee_datas(self, employee):
        self.main_grid_content.clear_widgets()

        employee = employee
        employee_id = employee.id
        employee_lastname = employee.lastname
        employee_firstname = employee.firstname
        employee_mail = employee.mail
        employee_role = employee.role

        if employee:
            label_file_title = MyButton(text="CLIENT ID : "+employee_id, size_hint=(1.0, 1.0), halign="left",
                                             valign="middle", padding=(10, 0), disabled=True)
            # self.label_file_title.font_size = self.label_file_title.width/1
            label_file_title.size_hint = (1, 0.1)
            label_file_title.pos_hint = {'center_x': 0.5, 'center_y': 0.96}

            label_lastname = MyButton(text="Nom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                           padding=(10, 0), disabled=True)
            label_lastname.size_hint = (0.3, 0.1)
            label_lastname.pos_hint = {'center_x': 0.15, 'center_y': 0.86}
            input_lastname = MyButton(text=employee_lastname, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                      valign="middle", disabled=True)
            input_lastname.size_hint = (0.7, 0.1)
            input_lastname.pos_hint = {'center_x': 0.65, 'center_y': 0.86}
            label_firstname = MyButton(text="Prénom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                            padding=(10, 0), disabled=True)
            label_firstname.size_hint = (0.3, 0.1)
            label_firstname.pos_hint = {'center_x': 0.15, 'center_y': 0.76}
            input_firstname = MyButton(text=employee_firstname, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                       valign="middle", disabled=True)
            input_firstname.size_hint = (0.7, 0.1)
            input_firstname.pos_hint = {'center_x': 0.65, 'center_y': 0.76}
            label_mail = MyButton(text="Adresse Mail :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
            label_mail.size_hint = (0.3, 0.1)
            label_mail.pos_hint = {'center_x': 0.15, 'center_y': 0.66}
            input_mail = MyButton(text=employee_mail, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                  valign="middle", disabled=True)
            input_mail.size_hint = (0.7, 0.1)
            input_mail.pos_hint = {'center_x': 0.65, 'center_y': 0.66}

            label_role = MyButton(text="Rôle :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
            label_role.size_hint = (0.3, 0.1)
            label_role.pos_hint = {'center_x': 0.15, 'center_y': 0.56}

            input_role = MyButton(text=employee_role, size_hint=(1.0, 1.0), padding=(10, 10), halign="left",
                                  valign="middle", disabled=True)
            input_role.size_hint = (0.7, 0.1)
            input_role.pos_hint = {'center_x': 0.65, 'center_y': 0.56}

            button_delete_employee = EmployeeButton(text="SUPPRIMER", size_hint=(0.20, 0.07), employee=employee)
            button_delete_employee.pos_hint = {'x': 0.8, 'y': 0.43}
            button_delete_employee.bind(on_release=self.cblk_delete_employee)

            button_validate = EmployeeButton(text="MODIFIER", size_hint=(0.15, 0.07), employee=employee)
            button_validate.pos_hint = {'x': 0.835, 'y': 0.05}
            button_validate.bind(on_release=self.cblk_modify_employee_file)

            self.main_grid_content.add_widget(label_file_title)
            self.main_grid_content.add_widget(label_lastname)
            self.main_grid_content.add_widget(input_lastname)
            self.main_grid_content.add_widget(label_firstname)
            self.main_grid_content.add_widget(input_firstname)
            self.main_grid_content.add_widget(label_mail)
            self.main_grid_content.add_widget(input_mail)
            self.main_grid_content.add_widget(label_role)
            self.main_grid_content.add_widget(input_role)
            self.main_grid_content.add_widget(button_delete_employee)
            self.add_widget(button_validate)

    def cblk_modify_employee_file(self, widget):
        self.controller.navigate_to_modify_employee_file(widget)

    def cblk_delete_employee(self, widget):
        self.controller.delete_employee(widget)

    def cblk_navigate_to_add_employee_file(self, widget):
        self.controller.navigate_to_add_employee_file()


class EmployeeFileController:
    def __init__(self, menus):
        self.menus = menus
        self.view = EmployeeFileView(self)
        self.employee = ""

    def navigate_to_add_employee_file(self):
        self.menus.menu_role_management.preload_list_employees()
        self.menus.display_menu(self.menus.menu_role_management)

    def navigate_to_modify_employee_file(self, widget):
        employee = widget.employee
        self.menus.file_employee_modify.preload_employee(employee)
        self.menus.display_menu(self.menus.file_employee_modify)

    def delete_employee(self, widget):
        employee_id = widget.employee.id
        path = './Datas/employees.json'
        path_db = './Datas/database.json'

        # prepare data for saving
        data = Employee.delete_employee(self, path, employee_id)
        data_login = Employee.delete_employee_login(self, path_db, employee_id)

        # saving as json
        try:
            self.save_as_json(path, data)
            self.save_as_json(path_db, data_login)
            self.display_pop_up_confirmation()
        except:
            pass

    def display_pop_up_confirmation(self):
        self.view.add_widget(self.view.label_popup)
        self.view.main_grid_content.clear_widgets()
        Clock.schedule_once(self.cblk_reload_form, 2)

    def cblk_reload_form(self, dt):
        self.view.remove_widget(self.view.label_popup)
        self.menus.menu_role_management.preload_list_employees()
        self.menus.display_menu(self.menus.menu_role_management)

    def save_as_json(self, path, data):
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)

    def preload_employee(self, employee):
        self.view.display_employee_datas(employee)
