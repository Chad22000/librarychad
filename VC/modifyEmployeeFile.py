from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
import kivy.utils as utils
from UI.UI import MyButton
from UI.UI import EmployeeButton
from UI.UI import BackgroundRoleManagement
from Model.Employee import Employee
from kivy.clock import Clock


class ModifyEmployeeFileView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.label_lastname = ""
        self.input_lastname = ""
        self.label_firstname = ""
        self.input_firstname = ""
        self.label_mail = ""
        self.input_mail = ""
        self.label_role = ""
        self.dropdown = ""
        self.mainbutton = ""
        self.modify_password_button = ""
        self.popup_password = ""
        self.label_password = ""
        self.input_password = ""
        self.label_new_password = ""
        self.input_new_password = ""
        self.label_confirm_password = ""
        self.input_confirm_password = ""

        # BACKGROUND MENU
        self.menu_background = BackgroundRoleManagement(text="", size_hint=(1, 1))

        # BACKGROUND MAIN GRID
        self.main_grid_background = Button(disabled=True, size_hint=(0.65, 0.39),
                                           pos_hint={'center_x': 0.5, 'center_y': 0.49}, background_normal="",
                                           background_color=utils.get_color_from_hex('#000000'))

        self.label_popup = Button(text="Modifications enregistrées !", disabled=True, background_color=[0.15, 0.7, 1,
                                                                                                        1])
        self.label_popup.size_hint = (0.7, 0.09)
        self.label_popup.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.main_grid = GridLayout(rows=3)
        self.main_grid.size_hint = (0.65, 0.7)
        self.main_grid.pos_hint = {'center_x': 0.5, 'center_y': 0.4}

        self.main_grid_content = FloatLayout()

        self.layout1()

    def layout1(self):
        self.clear_widgets()
        self.add_widget(self.menu_background)
        self.add_widget(self.main_grid_background)
        self.add_widget(self.main_grid)
        self.main_grid.add_widget(self.main_grid_content)

    def display_employee_datas(self, employee):
        self.main_grid_content.clear_widgets()

        employee = employee
        employee_id = employee.id
        employee_lastname = employee.lastname
        employee_firstname = employee.firstname
        employee_mail = employee.mail

        if employee:
            label_file_title = MyButton(text="CLIENT ID : "+employee_id, size_hint=(1.0, 1.0), halign="left",
                                             valign="middle", padding=(10, 0), disabled=True)
            # self.label_file_title.font_size = self.label_file_title.width/1
            label_file_title.size_hint = (1, 0.1)
            label_file_title.pos_hint = {'center_x': 0.5, 'center_y': 0.86}

            self.label_lastname = MyButton(text="Nom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                           padding=(10, 0), disabled=True)
            self.label_lastname.size_hint = (0.3, 0.1)
            self.label_lastname.pos_hint = {'center_x': 0.15, 'center_y': 0.76}
            self.input_lastname = TextInput(text=employee_lastname, size_hint=(1.0, 1.0), padding=(10, 10),
                                            multiline=False)
            self.input_lastname.size_hint = (0.7, 0.1)
            self.input_lastname.pos_hint = {'center_x': 0.65, 'center_y': 0.76}
            self.label_firstname = MyButton(text="Prénom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                            padding=(10, 0), disabled=True)
            self.label_firstname.size_hint = (0.3, 0.1)
            self.label_firstname.pos_hint = {'center_x': 0.15, 'center_y': 0.66}
            self.input_firstname = TextInput(text=employee_firstname, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_firstname.size_hint = (0.7, 0.1)
            self.input_firstname.pos_hint = {'center_x': 0.65, 'center_y': 0.66}

            self.label_mail = MyButton(text="Adresse Mail :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
            self.label_mail.size_hint = (0.3, 0.1)
            self.label_mail.pos_hint = {'center_x': 0.15, 'center_y': 0.56}
            self.input_mail = TextInput(text=employee_mail, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_mail.size_hint = (0.7, 0.1)
            self.input_mail.pos_hint = {'center_x': 0.65, 'center_y': 0.56}

            self.label_role = MyButton(text="Rôle :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
            self.label_role.size_hint = (0.3, 0.1)
            self.label_role.pos_hint = {'center_x': 0.15, 'center_y': 0.46}

            self.generate_dropdown(employee)

            self.modify_password_button = MyButton(text="Modifier le mot de passe", valign="center", halign="center",
                                                   padding=(10, 0), size_hint=(1, 0.075))
            self.modify_password_button.pos_hint = {'center_x': 0.5, 'center_y': 0.37}
            self.modify_password_button.bind(on_release=self.cblk_modify_password)

            box = GridLayout(cols=2, rows=4)

            self.label_password = MyButton(text="Mot de passe :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                           padding=(10, 0), disabled=True)
            self.label_password.size_hint = (0.3, 0.1)
            self.label_password.pos_hint = {'center_x': 0.15, 'center_y': 0.46}

            self.input_password = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10), password=True)
            self.input_password.size_hint = (0.7, 0.1)
            self.input_password.pos_hint = {'center_x': 0.65, 'center_y': 0.46}

            self.label_new_password = MyButton(text="Nouveau mot de passe :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                           padding=(10, 0), disabled=True)
            self.label_new_password.size_hint = (0.3, 0.1)
            self.label_new_password.pos_hint = {'center_x': 0.15, 'center_y': 0.46}

            self.input_new_password = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10), password=True)
            self.input_new_password.size_hint = (0.7, 0.1)
            self.input_new_password.pos_hint = {'center_x': 0.65, 'center_y': 0.46}

            self.label_confirm_password = MyButton(text="Confirmer Mot de pass :", size_hint=(1.0, 1.0), halign="right",
                                                   valign="middle", padding=(10, 0), disabled=True)
            self.label_confirm_password.size_hint = (0.3, 0.1)
            self.label_confirm_password.pos_hint = {'center_x': 0.15, 'center_y': 0.36}

            self.input_confirm_password = TextInput(text='', multiline=False, size_hint=(1.0, 1.0), padding=(10, 10), password=True)
            self.input_confirm_password.size_hint = (0.7, 0.1)
            self.input_confirm_password.pos_hint = {'center_x': 0.65, 'center_y': 0.36}

            self.popup_password = Popup(title="Modifier le mot de passe", size_hint=(0.8, 0.4), size_hint_max_x=700,
                                        size_hint_max_y=200, content=box)

            button_validate = EmployeeButton(text="ENREGISTRER", size_hint=(0.15, 0.07), employee=employee)
            button_validate.pos_hint = {'x': 0.835, 'y': 0.05}
            button_validate.bind(on_release=self.cblk_modify_employee_file)

            button_return = EmployeeButton(text="ANNULER", size_hint=(0.15, 0.07), employee=employee)
            button_return.pos_hint = {'x': 0.011, 'y': 0.05}
            button_return.bind(on_release=self.cblk_navigate_to_employee_file)

            self.main_grid_content.add_widget(label_file_title)
            self.main_grid_content.add_widget(self.label_lastname)
            self.main_grid_content.add_widget(self.input_lastname)
            self.main_grid_content.add_widget(self.label_firstname)
            self.main_grid_content.add_widget(self.input_firstname)
            self.main_grid_content.add_widget(self.label_mail)
            self.main_grid_content.add_widget(self.input_mail)
            self.main_grid_content.add_widget(self.label_role)
            self.main_grid_content.add_widget(self.mainbutton)
            self.main_grid_content.add_widget(self.modify_password_button)
            box.add_widget(self.label_password)
            box.add_widget(self.input_password)
            box.add_widget(self.label_new_password)
            box.add_widget(self.input_new_password)
            box.add_widget(self.label_confirm_password)
            box.add_widget(self.input_confirm_password)
            self.add_widget(button_validate)
            self.add_widget(button_return)

    def generate_dropdown(self, employee):
        # create a dropdown with 10 buttons
        liste_role = ["manager", "vendeur"]
        self.dropdown = DropDown()
        for item in liste_role:
            # When adding widgets, we need to specify the height manually
            # (disabling the size_hint_y) so the dropdown can calculate
            # the area it needs.

            btn = Button(text=item, size_hint_y=None, height=50)

            # for each button, attach a callback that will call the select() method
            # on the dropdown. We'll pass the text of the button as the data of the
            # selection.
            btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))

            # then add the button inside the dropdown
            self.dropdown.add_widget(btn)

        # create a big main button
        if employee.role:
            message_button = str(employee.role[0].lower() + employee.role[1:])
        else:
            message_button = 'Choisir'
        self.mainbutton = Button(text=message_button, size_hint=(1.0, 1.0))
        self.mainbutton.size_hint = (0.7, 0.1)
        self.mainbutton.pos_hint = {'center_x': 0.65, 'center_y': 0.46}

        # show the dropdown menu when the main button is released
        # note: all the bind() calls pass the instance of the caller (here, the
        # mainbutton instance) as the first argument of the callback (here,
        # dropdown.open.).
        self.mainbutton.bind(on_release=self.dropdown.open)

        # one last thing, listen for the selection in the dropdown list and
        # assign the data to the button text.
        self.dropdown.bind(on_select=lambda instance, x: setattr(self.mainbutton, 'text', x))


    def cblk_modify_employee_file(self, widget):
        widget.employee.id = widget.employee.id
        widget.employee.lastname = self.input_lastname.text
        widget.employee.firstname = self.input_firstname.text
        widget.employee.mail = self.input_mail.text
        widget.employee.role = self.mainbutton.text
        widget.employee.username = self.input_firstname.text + " " + self.input_lastname.text
        widget. employee.password = self.input_confirm_password.text

        self.controller.modify_employee_file(widget)

    def cblk_modify_password(self, widget):
        self.popup_password.open()

    def cblk_navigate_to_employee_file(self, widget):
        self.controller.navigate_to_employee_file(widget)


class ModifyEmployeeFileController:
    def __init__(self, menus):
        self.menus = menus
        self.view = ModifyEmployeeFileView(self)
        self.employee = ""

    def navigate_to_employee_file(self, widget):
        employee = widget.employee
        self.menus.file_employee.preload_employee(employee)
        self.menus.display_menu(self.menus.file_employee)

    def preload_employee(self, employee):
        self.view.display_employee_datas(employee)

    def modify_employee_file(self, widget):
        Employee.modify_employee(self, widget)
        self.display_pop_up_confirmation(widget)


    def display_pop_up_confirmation(self, widget):
        self.employee = widget.employee
        self.view.add_widget(self.view.label_popup)
        self.view.main_grid_content.clear_widgets()
        Clock.schedule_once(self.cblk_reload_form, 2)

    def get_employee_role(self):
        if self.view.mainbutton.text == "manager":
            role = True
            return role
        else:
            role = False
            return role

    def cblk_reload_form(self, dt):
        self.view.remove_widget(self.view.label_popup)
        self.menus.file_employee.preload_employee(self.employee)
        self.menus.display_menu(self.menus.file_employee)
