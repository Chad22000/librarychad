from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from Model.book import Book
from kivy.uix.textinput import TextInput
from Model.list import ListObject
from UI.UI import *
from kivy.uix.popup import Popup
import re


class MenuLibrary(FloatLayout):
    def __init__(self, controller, **kwargs):
        super().__init__()
        self.controller = controller
        self.controller.load_books()
        self.research_car = None
        ########################
        #     TITLE LAYOUT     #
        ########################
        # MAIN LABEL
        self.mainLabel = Label(text="Bibliothèque", size_hint_y=1, size_hint_x=1)
        self.mainLabel.pos_hint = {'x': 0, 'y': .42}
        ######################
        #  BOOK FILE LAYOUT  #
        ######################
        # BOOK FILE BACKGROUND
        self.book_file_background = Button(disabled=True, size_hint_y=.6, size_hint_x=.3)
        self.book_file_background.pos_hint = {'x': .68, 'y': .2}
        # BOOK TITLE
        self.book_title = Button(disabled=True, size_hint_y=.08, size_hint_x=.2)
        self.book_title.pos_hint = {'x': .69, 'y': .71}
        # BOOK AUTHOR
        self.book_author = Button(disabled=True, size_hint_y=.08, size_hint_x=.2)
        self.book_author.pos_hint = {'x': .69, 'y': .62}
        # BOOK PRICE
        self.book_price = Button(disabled=True, size_hint_y=.08, size_hint_x=.2)
        self.book_price.pos_hint = {'x': .69, 'y': .53}
        # BOOK SYNOPSIS
        self.book_synopsis = Button(halign='justify', disabled=True, size_hint_y=.24, size_hint_x=.2)
        self.book_synopsis.pos_hint = {'x': .69, 'y': .28}
        self.book_synopsis_text = Label(size_hint=(1, 1))
        self.book_synopsis_text.size_hint = (.2, .24)
        self.book_synopsis_text.pos_hint = {'x': .19, 'y': -.22}
        # BOOK AVAILABILITY
        self.book_availability = Button(disabled=True, size_hint_y=.06, size_hint_x=.20)
        self.book_availability.pos_hint = {'x': .73, 'y': .21}
        ########################
        #    RESEARCH LAYOUT   #
        ########################
        # RESEARCH BOOK LABEL
        self.research_book_label = Label(text="Rechercher un livre", size_hint_y=1, size_hint_x=1)
        self.research_book_label.pos_hint = {'x': -.35, 'y': .2}
        # RESEARCH BOOK INPUT
        self.research_book_input = TextInput(size_hint_y=.058, size_hint_x=.2)
        self.research_book_input.pos_hint = {'x': .05, 'y': .61}
        self.research_book_input.bind(text=self.input_research)
        ########################
        #  MANAGEMENT LAYOUT   #
        ########################
        # BUTTON BOOK FILE
        self.btn_book_file = Button(text="Fiche du livre", size_hint_y=.07, size_hint_x=.23)
        self.btn_book_file.pos_hint = {'x': 0.03, 'y': 0.5}
        self.btn_book_file.bind(on_release=self.clbk_book_file)
        # BUTTON CREATE BOOK
        self.btn_add_book = Button(text="Ajouter un livre", size_hint_y=.07, size_hint_x=.23)
        self.btn_add_book.pos_hint = {'x': 0.03, 'y': 0.4}
        self.btn_add_book.bind(on_release=self.clbk_add_book)
        # BUTTON MODIFY BOOK
        self.btn_modify_book = Button(text="Modifier les informations", size_hint_y=.07, size_hint_x=.23)
        self.btn_modify_book.pos_hint = {'x': 0.03, 'y': 0.3}
        self.btn_modify_book.bind(on_release=self.clbk_modify_book)
        # BUTTON ARCHIVE
        self.btn_archive = Button(text="Archives", size_hint_y=.07, size_hint_x=.23)
        self.btn_archive.pos_hint = {'x': 0.03, 'y': 0.2}
        self.btn_archive.bind(on_release=self.clbk_archive)
        ########################
        # SCROLL_VIEW LAYOUT   #
        ########################
        # SCROLL_VIEW LAYOUT + WIDGET
        self.scroll_layout = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout.bind(minimum_height=self.scroll_layout.setter('height'))
        # BUTTON IN SCROLL_VIEW
        self.scroll_view = ScrollView(size_hint=(0.37, .6), size=(Window.width, Window.height))
        self.scroll_view.pos_hint = {'x': .3, 'y': .2}
        self.scroll_view.add_widget(self.scroll_layout)
        self.list_book_button()
        ########################
        #        FOOTER        #
        ########################
        # BUTTON CANCEL
        self.btn_cancel = Button(text="Retour", size_hint_y=.07, size_hint_x=.25)
        self.btn_cancel.pos_hint = {'x': 0.02, 'y': 0.02}
        self.btn_cancel.bind(on_release=self.cblk_previous)
        ########################

        # METHOD CALLING ALL DISPLAY
        self.layout()

    # INITIALISATION METHOD FOR ALL WIDGETS FROM THE CLASS LAYOUT
    def layout(self):
        self.clear_widgets()
        self.add_widget(self.mainLabel)
        self.add_widget(self.book_file_background)
        self.add_widget(self.book_title)
        self.add_widget(self.book_author)
        self.add_widget(self.book_price)
        self.add_widget(self.book_synopsis)
        self.add_widget(self.book_availability)
        self.add_widget(self.scroll_view)
        self.add_widget(self.btn_cancel)
        self.add_widget(self.research_book_label)
        self.add_widget(self.research_book_input)
        self.add_widget(self.btn_book_file)
        self.add_widget(self.btn_add_book)
        self.add_widget(self.btn_modify_book)
        self.add_widget(self.book_synopsis_text)
        self.add_widget(self.btn_archive)

    def list_book_button(self):
        self.scroll_layout.clear_widgets()
        for book in self.controller.list_books:
            button_text = book.author + " " + book.name
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            btn.bind(on_press=self.on_press_customer)
            self.scroll_layout.add_widget(btn)

    def input_research(self, widget, input_text):
        list_book = self.controller.input_research(widget)
        self.update_list(list_book)

    def update_list(self, book_list):
        self.scroll_layout.clear_widgets()
        for book in book_list:
            button_text = book.author + " " + book.name
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            btn.bind(on_press=self.on_press_customer)
            self.scroll_layout.add_widget(btn)

    def on_press_customer(self, widget):
        self.update_toggle(widget)
        self.file_book_charged(widget)

    def file_book_charged(self, widget):
        self.controller.file_book_charged(widget.book)

    def update_toggle(self, widget):
        for button in self.scroll_layout.children:
            if widget.book != button.book and button.state == "down":
                button.state = "normal"

    def cblk_previous(self, widget):
        self.controller.previous()

    def cblk_research_book(self, widget):
        self.controller.research_book(widget)

    def cblk_research_customer(self, widget):
        self.controller.research_customer(widget)

    def clbk_book_file(self, widget):
        book = self.book_checked()
        self.controller.move_to_book_file(book)

    def clbk_add_book(self, widget):
        self.controller.add_book()

    def book_checked(self):
        for button in self.scroll_layout.children:
            if button.state == "down":
                return button.book

    def clbk_modify_book(self, widget):
        book = self.book_checked()
        self.controller.modify_book(book)

    def clbk_archive(self, widget):
        self.controller.move_to_archive()

    def pop_up_no_selection(self):
        layout_pop_up = FloatLayout()
        message = Label(text="Veuillez sélectionner un livre")
        message.pos_hint = {'x': 0, 'y': 0.2}
        layout_pop_up.add_widget(message)
        popup = Popup(title="ATTENTION", content=layout_pop_up, auto_dismiss=True, size_hint=(0.3, .2))
        popup.open()


class MenuLibraryController:
    def __init__(self, menus):
        self.menus = menus
        self.previous_page = ""
        self.list_books = []
        self.view = MenuLibrary(self)

    def load_books(self):
        list_books = ListObject()
        self.list_books = list_books.load_list_of_books()

    def input_research(self, widget):
        input_text = widget.text
        list_research = []
        for book in self.list_books:
            lastname = book.name
            firstname = book.author
            if re.match(input_text, lastname) \
                    or re.match(input_text, firstname)\
                    or re.match(input_text, lastname+" "+firstname) \
                    or re.match(input_text, firstname+" "+lastname):
                list_research.append(book)
        return list_research

    def file_book_charged(self, book):
        self.view.book_author.text = book.author
        self.view.book_title.text = book.name
        self.view.book_price.text = book.price
        self.view.book_synopsis_text.text = book.abstract
        self.view.book_availability.text = book.r_availability()

    def previous(self):
        self.menus.display_menu(self.menus.menu_main)

    def move_to_book_file(self, book):
        if book:
            self.menus.book_file.charge_data(book)
            self.menus.display_menu(self.menus.book_file)
        else:
            self.view.pop_up_no_selection()

    def add_book(self):
        self.menus.display_menu(self.menus.add_book)

    def modify_book(self, book):
        self.menus.modify_book.charge_data(book)
        self.menus.display_menu(self.menus.modify_book)

    def move_to_archive(self):
        self.menus.display_menu(self.menus.archive)

