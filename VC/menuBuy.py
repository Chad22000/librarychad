from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.core.window import Window
from kivy.uix.label import Label
from UI.UI import *
from datetime import datetime
from kivy.uix.popup import Popup
from Model.Sale import Sale


class MenuBuy(FloatLayout):
    def __init__(self, controller, **kwargs):
        super().__init__()
        self.controller = controller
        ########################
        #     TITLE LAYOUT     #
        ########################
        # MAIN LABEL
        self.mainLabel = Label(text="Achat de livre", size_hint_y=1, size_hint_x=1)
        self.mainLabel.pos_hint = {'x': 0, 'y': .42}
        ########################
        #   RESEARCH BUTTON    #
        ########################
        # RESEARCH BOOK BUTTON
        self.btn_search_book = PageIDButton(text="Rechercher un livre", size_hint_y=.07, size_hint_x=.3, pageID="menuBuy")
        self.btn_search_book.pos_hint = {'x': .25, 'y': .8}
        self.btn_search_book.bind(on_release=self.cblk_research_book)
        # RESEARCH CUSTOMER BUTTON
        self.btn_search_customer = PageIDButton(text="Rechercher un client", size_hint_y=.07, size_hint_x=.25, pageID="menuBuy")
        self.btn_search_customer.pos_hint = {'x': .65, 'y': .8}
        self.btn_search_customer.bind(on_release=self.cblk_research_customer)
        ########################
        #   BOOK FILE LAYOUT   #
        ########################
        # BACKGROUND
        book_file = Button(size_hint_y=.68, size_hint_x=.31, disabled=True)
        book_file.pos_hint = {'x': .245, 'y': .195}
        self.add_widget(book_file)
        # LASTNAME LABEL
        self.book_name = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.book_name.pos_hint = {'x': .25, 'y': .695}
        # FIRSTNAME LABEL
        self.book_author = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.book_author.pos_hint = {'x': .25, 'y': .590}
        # ADDRESS LABEL
        self.book_price = Button(size_hint_y=.175, size_hint_x=.296, disabled=True)
        self.book_price.pos_hint = {'x': .25, 'y': .2}
        # POSTAL CODE LABEL
        self.book_image = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.book_image.pos_hint = {'x': .25, 'y': .380}

        ########################
        # CUSTOMER FILE LAYOUT #
        ########################
        # BACKGROUND
        customer_file = Button(size_hint_y=.68, size_hint_x=.305, disabled=True)
        customer_file.pos_hint = {'x': .625, 'y': .195}
        self.add_widget(customer_file)
        # LASTNAME LABEL
        self.customer_lastname = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_lastname.pos_hint = {'x': .63, 'y': .695}
        # FIRSTNAME LABEL
        self.customer_firstname = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_firstname.pos_hint = {'x': .63, 'y': .590}
        # ADDRESS LABEL
        self.customer_address = Button(size_hint_y=.175, size_hint_x=.296, disabled=True)
        self.customer_address.pos_hint = {'x': .63, 'y': .2}
        # POSTAL CODE LABEL
        self.customer_postal_code = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_postal_code.pos_hint = {'x': .63, 'y': .380}
        # CITY LABEL
        self.customer_city = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_city.pos_hint = {'x': .63, 'y': .485}
        ########################
        # SCROLL_VIEW LAYOUT   #
        ########################
        # SCROLL_VIEW LABEL
        self.label_scroll = Label(text="Liste de livres:", size_hint_y=1, size_hint_x=1)
        self.label_scroll.pos_hint = {'x': -.4, 'y': .3}
        # SCROLL_VIEW LAYOUT + WIDGET
        self.scroll_layout = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout.bind(minimum_height=self.scroll_layout.setter('height'))
        # BUTTON IN SCROLL_VIEW
        self.scroll_view = ScrollView(size_hint=(0.15, .2), size=(Window.width, Window.height))
        self.scroll_view.pos_hint = {'x': .03, 'y': .57}
        self.scroll_view.add_widget(self.scroll_layout)
        # LIST BUTTON IN SCROLL_VIEW
        self.list_book_button()
        # SCROLL_VIEW LABEL
        self.label_bill = Label(text="Facturation:", size_hint_y=1, size_hint_x=1)
        self.label_bill.pos_hint = {'x': -.4, 'y': 0}
        ########################
        #        FOOTER        #
        ########################
        # ACTIVATE SCAN BUTTON
        self.btn_scan = Button(text="Activer le scan", size_hint_y=.15, size_hint_x=.2)
        self.btn_scan.pos_hint = {'x': 0.3, 'y': 0.02}
        # INPUT SCAN LABEL
        self.label_input_scan = Label(text="Entrer le code du livre:")
        self.label_input_scan.pos_hint = {'x': .1, 'y': -.34}
        # INPUT SCAN DIGIT
        self.input_scan = TextInput(size_hint_y=.05, size_hint_x=.15)
        self.input_scan.pos_hint = {'x': 0.53, 'y': 0.09}
        # BUTTON CANCEL
        self.btn_cancel = Button(text="Retour", size_hint_y=.07, size_hint_x=.25)
        self.btn_cancel.pos_hint = {'x': 0.02, 'y': 0.02}
        self.btn_cancel.bind(on_release=self.cblk_previous)
        # BUTTON ACCEPT / TERMINATE
        self.btn_accept = Button(text="Confirmer", size_hint_y=.07, size_hint_x=.25)
        self.btn_accept.pos_hint = {'x': .73, 'y': 0.02}
        self.btn_accept.bind(on_release=self.cblk_finish)
        ########################

        # METHOD CALLING ALL DISPLAY
        self.layout()

    # INITIALISATION METHOD FOR ALL WIDGETS FROM THE CLASS LAYOUT
    def layout(self):
        self.add_widget(self.mainLabel)
        self.add_widget(self.label_scroll)
        self.add_widget(self.btn_search_book)
        self.add_widget(self.btn_search_customer)
        self.add_widget(self.scroll_view)
        self.add_widget(self.btn_accept)
        self.add_widget(self.btn_cancel)
        self.add_widget(self.btn_scan)
        self.add_widget(self.input_scan)
        self.add_widget(self.label_input_scan)
        self.add_widget(self.label_bill)

        # BOOK LAYOUT
        self.add_widget(self.book_author)
        self.add_widget(self.book_name)
        self.add_widget(self.book_price)
        self.add_widget(self.book_image)
        # CUSTOMER LAYOUT
        self.add_widget(self.customer_lastname)
        self.add_widget(self.customer_firstname)
        self.add_widget(self.customer_city)
        self.add_widget(self.customer_postal_code)
        self.add_widget(self.customer_address)

    def on_press_book(self, widget):
        self.update_toggle(widget)
        self.charge_book()

    def update_toggle(self, widget):
        for button in self.scroll_layout.children:
            if widget.book != button.book and button.state == "down":
                button.state = "normal"

    def book_checked(self):
        for button in self.scroll_layout.children:
            if button.state == "down":
                return button.book

    def update_list_button(self):
        self.scroll_layout.clear_widgets()
        self.list_book_button()

    def list_book_button(self):
        for book in self.controller.list_books_buy:
            button_text = book.author + " " + book.name
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            btn.bind(on_press=self.on_press_book)
            self.scroll_layout.add_widget(btn)

    def update_customer_file(self):
        customer = self.controller.customer
        if customer:
            self.customer_firstname.text = str("Prenom : ")+customer.firstname
            self.customer_lastname.text = str("Nom : ")+customer.lastname
            self.customer_city.text = str("Ville : ")+customer.city
            self.customer_postal_code.text = str("Code Postal : ")+customer.zipcode
            self.customer_address.text = str("Adress : ")+customer.address
        else:
            self.customer_firstname.text = ""
            self.customer_lastname.text = ""
            self.customer_city.text = ""
            self.customer_postal_code.text = ""
            self.customer_address.text = ""

    def charge_book(self):
        book = self.book_checked()
        if book:
            self.book_name.text = book.name
            self.book_author.text = book.author
            self.book_price.text = book.price
            self.book_image.text = book.category
        else:
            self.book_name.text = ""
            self.book_author.text = ""
            self.book_price.text = ""
            self.book_image.text = ""

    def pop_up_out_of_stock(self):
        layout_pop_up = FloatLayout()
        message = Label(text="LIVRE EN RUPTURE DE STOCK\n VENTE SUSPENDU")
        message.pos_hint = {'x': 0, 'y': 0.3}
        layout_pop_up.add_widget(message)
        popup = Popup(title="ATTENTION", content=layout_pop_up, auto_dismiss=True, size_hint=(0.3, .4))
        popup.open()

    def cblk_previous(self, widget):
        self.controller.previous()

    def cblk_finish(self, widget):
        self.controller.finish()

    def cblk_research_book(self, widget):
        self.controller.research_book(widget)

    def cblk_research_customer(self, widget):
        self.controller.research_customer(widget)


class MenuBuyController:
    def __init__(self, menus):
        self.menus = menus
        self.list_books_buy = []
        self.customer = ""
        self.book = ""
        self.view = MenuBuy(self)

    def finish(self):
        if self.save_availibility():
            self.save_in_book()
            self.save_in_history()
            self.reset_all_variable()
            self.view.update_list_button()
            self.view.charge_book()
            self.view.update_customer_file()
            self.menus.display_menu(self.menus.menu_main)

    def reset_all_variable(self):
        self.list_books_buy = []
        self.customer = ""

    def save_in_book(self):
        for book in self.list_books_buy:
            book.change_availibility()
            book.save_availibility()

    def save_availibility(self):
        for book in self.list_books_buy:
            if book.availability:
                pass
            else:
                self.view.pop_up_out_of_stock()
                return False
        return True

    def previous(self):
        self.list_books_buy = []
        self.customer = ""
        self.view.update_list_button()
        self.view.update_customer_file()
        self.menus.display_menu(self.menus.menu_main)

    def research_book(self, widget):
        self.menus.researchBook.previouspage(widget.pageID)
        self.menus.display_menu(self.menus.researchBook)

    def research_customer(self, widget):
        self.menus.researchCustomer.previouspage(widget.pageID)
        self.menus.display_menu(self.menus.researchCustomer)

    def charge_data(self, book):
        self.list_books_buy.append(book)
        self.view.update_list_button()

    def charge_customer(self, customer):
        self.customer = customer
        self.view.update_customer_file()

    def save_in_history(self):
        today = datetime.now()
        list = self.list_id_books()
        price = self.total_price()
        sale = Sale(list_books=list, amount=price, client_id=self.customer.id, sales_date=today.strftime('%d/%m/%Y'))
        sale.id = sale.set_sale_history_id()
        sale.save_sale()

    def list_id_books(self):
        list_id = []
        for book in self.list_books_buy:
            list_id.append(book.id)
        return list_id

    def total_price(self):
        price = ""
        for book in self.list_books_buy:
            price = price+book.price
        return price