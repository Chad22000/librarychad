from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
import kivy.utils as utils
from UI.UI import MyButton
from UI.UI import CustomerButton
from UI.UI import BackgroundCustomerManagement
import json
from kivy.clock import Clock


class ModifyCustomerFileView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.label_lastname = ""
        self.input_lastname = ""
        self.label_firstname = ""
        self.input_firstname = ""
        self.label_address = ""
        self.input_address = ""
        self.label_zipcode = ""
        self.input_zipcode = ""
        self.label_city = ""
        self.input_city = ""
        self.label_phone = ""
        self.input_phone = ""
        self.label_mobile = ""
        self.input_mobile = ""
        self.label_mail = ""
        self.input_mail = ""

        # BACKGROUND MENU
        self.menu_background = BackgroundCustomerManagement(text="", size_hint=(1, 1))

        # BACKGROUND MAIN GRID
        self.main_grid_background = Button(disabled=True, size_hint=(0.65, 0.81),
                                           pos_hint={'center_x': 0.5, 'center_y': 0.55}, background_normal="",
                                           background_color=utils.get_color_from_hex('#000000'))

        self.label_popup = Button(text="Modifications enregistrées !", disabled=True, background_color=[0.15, 0.7, 1,
                                                                                                        1])
        self.label_popup.size_hint = (0.7, 0.09)
        self.label_popup.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.main_grid = GridLayout(rows=3)
        self.main_grid.size_hint = (0.65, 0.90)
        self.main_grid.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.main_grid_content = FloatLayout()

        self.layout1()

    def layout1(self):
        self.clear_widgets()
        self.add_widget(self.menu_background)
        self.add_widget(self.main_grid_background)
        self.add_widget(self.main_grid)
        self.main_grid.add_widget(self.main_grid_content)

    def display_customer_datas(self, customer):
        self.main_grid_content.clear_widgets()
        customer = customer
        customer_id = customer.id
        customer_lastname = customer.lastname
        customer_firstname = customer.firstname
        customer_address = customer.address
        customer_zipcode = customer.zipcode
        customer_city = customer.city
        customer_phone = customer.phone
        customer_mobile = customer.mobile
        customer_mail = customer.mail

        if customer:
            label_file_title = MyButton(text="CLIENT ID : "+customer_id, size_hint=(1.0, 1.0), halign="left",
                                             valign="middle", padding=(10, 0), disabled=True)
            # self.label_file_title.font_size = self.label_file_title.width/1
            label_file_title.size_hint = (1, 0.1)
            label_file_title.pos_hint = {'center_x': 0.5, 'center_y': 0.96}

            self.label_lastname = MyButton(text="Nom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                           padding=(10, 0), disabled=True)
            self.label_lastname.size_hint = (0.3, 0.1)
            self.label_lastname.pos_hint = {'center_x': 0.15, 'center_y': 0.86}
            self.input_lastname = TextInput(text=customer_lastname, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_lastname.size_hint = (0.7, 0.1)
            self.input_lastname.pos_hint = {'center_x': 0.65, 'center_y': 0.86}
            self.label_firstname = MyButton(text="Prénom :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                            padding=(10, 0), disabled=True)
            self.label_firstname.size_hint = (0.3, 0.1)
            self.label_firstname.pos_hint = {'center_x': 0.15, 'center_y': 0.76}
            self.input_firstname = TextInput(text=customer_firstname, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_firstname.size_hint = (0.7, 0.1)
            self.input_firstname.pos_hint = {'center_x': 0.65, 'center_y': 0.76}
            self.label_address = MyButton(text="Adresse :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                          padding=(10, 0), disabled=True)
            self.label_address.size_hint = (0.3, 0.1)
            self.label_address.pos_hint = {'center_x': 0.15, 'center_y': 0.66}
            self.input_address = TextInput(text=customer_address, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_address.size_hint = (0.7, 0.1)
            self.input_address.pos_hint = {'center_x': 0.65, 'center_y': 0.66}

            self.label_zipcode = MyButton(text="Code Postal :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                          padding=(10, 0), disabled=True)
            self.label_zipcode.size_hint = (0.3, 0.1)
            self.label_zipcode.pos_hint = {'center_x': 0.15, 'center_y': 0.56}
            self.input_zipcode = TextInput(text=customer_zipcode, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_zipcode.size_hint = (0.7, 0.1)
            self.input_zipcode.pos_hint = {'center_x': 0.65, 'center_y': 0.56}
            self.label_city = MyButton(text="City :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
            self.label_city.size_hint = (0.3, 0.1)
            self.label_city.pos_hint = {'center_x': 0.15, 'center_y': 0.46}
            self.input_city = TextInput(text=customer_city, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_city.size_hint = (0.7, 0.1)
            self.input_city.pos_hint = {'center_x': 0.65, 'center_y': 0.46}
            self.label_phone = MyButton(text="Téléphone :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                        padding=(10, 0), disabled=True)
            self.label_phone.size_hint = (0.3, 0.1)
            self.label_phone.pos_hint = {'center_x': 0.15, 'center_y': 0.36}
            self.input_phone = TextInput(text=customer_phone, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_phone.size_hint = (0.7, 0.1)
            self.input_phone.pos_hint = {'center_x': 0.65, 'center_y': 0.36}
            self.label_mobile = MyButton(text="Mobile :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                         padding=(10, 0), disabled=True)
            self.label_mobile.size_hint = (0.3, 0.1)
            self.label_mobile.pos_hint = {'center_x': 0.15, 'center_y': 0.26}
            self.input_mobile = TextInput(text=customer_mobile, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_mobile.size_hint = (0.7, 0.1)
            self.input_mobile.pos_hint = {'center_x': 0.65, 'center_y': 0.26}

            self.label_mail = MyButton(text="Adresse Mail :", size_hint=(1.0, 1.0), halign="right", valign="middle",
                                       padding=(10, 0), disabled=True)
            self.label_mail.size_hint = (0.3, 0.1)
            self.label_mail.pos_hint = {'center_x': 0.15, 'center_y': 0.16}
            self.input_mail = TextInput(text=customer_mail, multiline=False, size_hint=(1.0, 1.0), padding=(10, 10))
            self.input_mail.size_hint = (0.7, 0.1)
            self.input_mail.pos_hint = {'center_x': 0.65, 'center_y': 0.16}

            button_validate = CustomerButton(text="ENREGISTRER", size_hint=(0.15, 0.07), customer=customer)
            button_validate.pos_hint = {'x': 0.835, 'y': 0.05}
            button_validate.bind(on_release=self.cblk_modify_customer_file)

            button_return = CustomerButton(text="ANNULER", size_hint=(0.15, 0.07), customer=customer)
            button_return.pos_hint = {'x': 0.011, 'y': 0.05}
            button_return.bind(on_release=self.cblk_navigate_to_customer_file)

            self.main_grid_content.add_widget(label_file_title)
            self.main_grid_content.add_widget(self.label_lastname)
            self.main_grid_content.add_widget(self.input_lastname)
            self.main_grid_content.add_widget(self.label_firstname)
            self.main_grid_content.add_widget(self.input_firstname)
            self.main_grid_content.add_widget(self.label_address)
            self.main_grid_content.add_widget(self.input_address)
            self.main_grid_content.add_widget(self.label_zipcode)
            self.main_grid_content.add_widget(self.input_zipcode)
            self.main_grid_content.add_widget(self.label_city)
            self.main_grid_content.add_widget(self.input_city)
            self.main_grid_content.add_widget(self.label_phone)
            self.main_grid_content.add_widget(self.input_phone)
            self.main_grid_content.add_widget(self.label_mobile)
            self.main_grid_content.add_widget(self.input_mobile)
            self.main_grid_content.add_widget(self.label_mail)
            self.main_grid_content.add_widget(self.input_mail)
            self.add_widget(button_validate)
            self.add_widget(button_return)

    def cblk_modify_customer_file(self, widget):
        widget.customer.id = widget.customer.id
        widget.customer.lastname = self.input_lastname.text
        widget.customer.firstname = self.input_firstname.text
        widget.customer.address = self.input_address.text
        widget.customer.zipcode = self.input_zipcode.text
        widget.customer.city = self.input_city.text
        widget.customer.phone = self.input_phone.text
        widget.customer.mobile = self.input_mobile.text
        widget.customer.mail = self.input_mail.text

        self.controller.modify_customer_file(widget)

    def cblk_navigate_to_customer_file(self, widget):
        self.controller.navigate_to_customer_file(widget)


class ModifyCustomerFileController:
    def __init__(self, menus):
        self.menus = menus
        self.view = ModifyCustomerFileView(self)
        self.customer = ""

    def navigate_to_customer_file(self, widget):
        customer = widget.customer
        self.menus.file_customer.preload_customer(customer)
        self.menus.display_menu(self.menus.file_customer)

    def preload_customer(self, customer):
        self.view.display_customer_datas(customer)

    def modify_customer_file(self, widget):
        # get new_customer datas from add customer form
        customer_id = widget.customer.id
        customer_lastname = widget.customer.lastname
        customer_firstname = widget.customer.firstname
        customer_address = widget.customer.address
        customer_zipcode = widget.customer.zipcode
        customer_city = widget.customer.city
        customer_phone = widget.customer.phone
        customer_mobile = widget.customer.mobile
        customer_mail = widget.customer.mail
        # opening json file
        path = './Datas/customers.json'
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['customers']

            for item in temp:
                if item['id'] == customer_id:
                    item['id'] = customer_id
                    item['lastname'] = customer_lastname
                    item['firstname'] = customer_firstname
                    item['address'] = customer_address
                    item['zipcode'] = customer_zipcode
                    item['city'] = customer_city
                    item['phone'] = customer_phone
                    item['mobile'] = customer_mobile
                    item['mail'] = customer_mail

            try:
                self.save_as_json(path, data)
                self.display_pop_up_confirmation(widget)
            except:
                pass

    def save_as_json(self, path, data):
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)

    def display_pop_up_confirmation(self, widget):
        self.customer = widget.customer
        self.view.add_widget(self.view.label_popup)
        self.view.main_grid_content.clear_widgets()
        Clock.schedule_once(self.cblk_reload_form, 2)

    def cblk_reload_form(self, dt):
        self.view.remove_widget(self.view.label_popup)
        self.menus.file_customer.preload_customer(self.customer)
        self.menus.display_menu(self.menus.file_customer)
