import os
os.environ['KIVY_GL_BACKEND'] = 'sdl2'
from kivy.app import App
from Model.loadData import LoadData
from kivy.uix.floatlayout import FloatLayout
from VC.header import HeaderController
from VC.mainMenu import MainMenuController
from VC.connectionView import MenuConnectionController
from VC.menuBorrow import MenuBorrowController
from VC.customerManagementMenu import CustomerManagementMenuController
from VC.addCustomerFile import AddCustomerFileController
from VC.customerFile import CustomerFileController
from VC.modifyCustomerFile import ModifyCustomerFileController
from VC.researchCustomer import ResearchCustomerController
from VC.researchBook import ResearchBookController
from VC.menuBuy import MenuBuyController
from VC.ManagementSaleBorrowing import AddManagementSaleBorrowing
from VC.roleManagementMenu import RoleManagementMenuController
from VC.employeeFile import EmployeeFileController
from VC.modifyEmployeeFile import ModifyEmployeeFileController
from VC.addEmployeeFile import AddEmployeeFileController
from VC.menuLibrary import MenuLibraryController
from VC.bookFile import BookFileController
from VC.modifyBookFile import ModifyBookFileController
from VC.addBookFile import AddBookFileController
from VC.archive import ArchiveController

##test
class LoadDatas:
    def __init__(self):
        self.datas = LoadData()
        self.datas.load_customers()
        self.datas.load_categories()
        self.datas.load_types()
        self.datas.load_books()
        self.datas.load_employees()
        self.datas.sales_history()
        self.datas.borrows_history()


# ---- #
class Window(FloatLayout):
    def __init__(self, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.layout = FloatLayout()
        self.layout.size_hint_y = 0.93
        self.add_widget(self.layout)

    def display_header(self, menu):
        menu.size_hint_y = 0.07
        menu.pos_hint = {"y": 0.93}
        self.add_widget(menu)

    def display_menu(self, menu):
        self.layout.clear_widgets()
        self.layout.add_widget(menu)


class Menus:
    def __init__(self, **kwargs):
        self.window = Window()
        self.header = HeaderController(self)
        self.menu_connection = MenuConnectionController(self)
        self.menu_main = MainMenuController(self)
        self.menu_borrow = MenuBorrowController(self)
        self.menu_buy = MenuBuyController(self)
        self.menu_role_management = RoleManagementMenuController(self)
        self.menu_library = MenuLibraryController(self)
        self.menu_customer_management = CustomerManagementMenuController(self)
        self.researchBook = ResearchBookController(self)
        self.researchCustomer = ResearchCustomerController(self)
        self.menu_customer_management = CustomerManagementMenuController(self)
        self.file_add_customer = AddCustomerFileController(self)
        self.file_customer = CustomerFileController(self)
        self.file_customer_modify = ModifyCustomerFileController(self)
        self.file_employee = EmployeeFileController(self)
        self.file_employee_modify = ModifyEmployeeFileController(self)
        self.file_add_employee = AddEmployeeFileController(self)
        self.managementsaleborrowing = AddManagementSaleBorrowing(self)
        self.book_file = BookFileController(self)
        self.modify_book = ModifyBookFileController(self)
        self.add_book = AddBookFileController(self)
        self.archive = ArchiveController(self)
        self.window.display_header(self.header.view)
        self.window.display_menu(self.menu_connection.view)


    def display_menu(self, menu):
        self.window.display_menu(menu.view)


class LibraryApp(App):

    def build(self):
        LoadDatas()
        menus = Menus()
        return menus.window


if __name__ == "__main__":
    LibraryApp().run()
