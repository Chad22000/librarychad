from Model.Employee import Employee


class Admin(Employee):
    def __init__(self, lastname, name, mail, employee_id, manager, password):
        Employee.__init__(self, lastname, name, mail, employee_id, manager, password)

    #TODO: change the role of admin
    def change_role(self):
        pass

    #TODO: creation of admin
    def create_admin(self):
        pass

    #TODO: modification of admin status
    def modify_admin(self):
        pass

    #TODO: deletion of admin in database
    def delete_admin(self):
        pass

    ##Commentaire ajoutéetest-sonarqube
    ## test sonar qube