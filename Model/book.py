import json
import os

class Book:
    def __init__(self, id, barcode, author, name, type, price, category, abstract, availability):
        self.id = id
        self.barcode = barcode
        self.author = author
        self.name = name
        self.type = type
        self.price = price
        self.category = category
        self.abstract = abstract
        self.availability = availability

    # RETURN STR() VALUE INSTEAD OF BOOL TO ATTRIBUTE AVAILIBILITY
    def r_availability(self):
        if self.availability:
            return "Disponible"
        else:
            return "Rupture de stock"

    # CHANGE BOOL VALUE OF AVAILIBILITY FROM TRUE TO FALSE
    def change_availibility(self):
        if self.availability:
            self.availability = False

    # SAVE DATA INSTANCE BOOK INTO JSON FILE
    def save_book(self):
        path = './Datas/books.json'
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['books']
            # PREPARATION DE L'INSTANCE A SAUVEGARDER
            new_data = {
                "id": self.id,
                "bar code": "",
                "author": self.author,
                "name": self.name,
                "type": self.type,
                "price": self.price,
                "category": self.category,
                "abstract": self.abstract,
                "availability": True
            }
            temp.append(new_data)
        self.save_as_json(data)

    # SET INSTANCE ID FROM LAST SAVED ID IN JSON FILE
    def set_book_id(self, path):
        file_object = open(path, "r")
        json_content = file_object.read()
        users_list = json.loads(json_content)
        nb_users = len(users_list['books'])
        user_id = str(nb_users + 1)
        file_object.close()
        return user_id

    # DUMP DATA INTO JSON FILE
    def save_as_json(self, data):
        path = './Datas/books.json'
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)

    def save_availibility(self):
        path = "./Datas/books.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                books_list = json_object['books']
                for book in books_list:
                    if book["id"] == self.id:
                        book['availability'] = False
            self.save_as_json(json_object)
