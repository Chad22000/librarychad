import json
import os


class Sale:
    def __init__(self, id=None, list_books=[], amount='', client_id='', sales_date=''):
        self.id = id
        self.list_book_id = list_books
        self.amount = amount
        self.client_id = client_id
        self.sales_date = sales_date

    def save_sale(self):
        path = './Datas/sales_history.json'
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['sales_history']
            # préparation des données de la catégorie
            new_data = {
                "id": self.id,
                "list_book_id": self.list_book_id,
                "amount": self.amount,
                "customer_id": self.client_id,
                "transaction_date": self.sales_date,
            }
            temp.append(new_data)
        self.save_as_json(data)

    def set_sale_history_id(self):
        path = './Datas/sales_history.json'
        file_object = open(path, "r")
        json_content = file_object.read()
        borrows_history_list = json.loads(json_content)
        nb_users = len(borrows_history_list['sales_history'])
        user_id = str(nb_users + 1)
        file_object.close()
        return user_id

    def save_as_json(self, data):
        path = './Datas/sales_history.json'
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4, default=str)


    def nbr_sale(self):
        pass
