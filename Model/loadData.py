import os
import json
import csv

class LoadData:
    def __init__(self):
        self.categories = []
        self.types = []

    def load_categories(self):
        # creating json file if doesn't exists
        path = "./Datas/categories.json"
        isExist = os.path.exists(path)

        if isExist:
            pass
        else:
            # create JSON file
            # Data to be written
            json_file = {
                "categories": [

                ]
            }
            # Serializing json
            json_object = json.dumps(json_file, indent=4)
            # Writing file
            with open(path, "w") as outfile:
                outfile.write(json_object)

            # opening json file
            with open(path, 'rb') as file:
                data = json.load(file)
                temp = data['categories']

                # loading customers datas from csv
                csv_path = "./Datas/Categories livres.csv"
                with open(csv_path, encoding="utf8") as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=';')

                    for row in spamreader:
                        if row[1] == 'Label':
                            continue
                        category_id = row[0]
                        category_label = row[1]

                        # préparation des données de la catégorie
                        new_data = {
                            "id": category_id,
                            "label": category_label,
                        }

                        temp.append(new_data)

                        self.save_as_json(path, data)

    def load_types(self):
        # creating json file if doesn't exists
        path = "./Datas/types.json"
        isExist = os.path.exists(path)

        if isExist:
            pass
        else:
            # create JSON file
            # Data to be written
            json_file = {
                "types": [

                ]
            }
            # Serializing json
            json_object = json.dumps(json_file, indent=4)
            # Writing file
            with open(path, "w") as outfile:
                outfile.write(json_object)

            # opening json file
            with open(path, 'rb') as file:
                data = json.load(file)
                temp = data['types']

                # loading customers datas from csv
                csv_path = "./Datas/Types livres.csv"
                with open(csv_path, encoding="utf8") as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=';')

                    for row in spamreader:
                        if row[1] == 'Label':
                            continue
                        type_id = row[0]
                        type_label = row[1]

                        # préparation des données de la catégorie
                        new_data = {
                            "id": type_id,
                            "label": type_label,
                        }

                        temp.append(new_data)

                        self.save_as_json(path, data)

    def load_customers(self):
        # creating json file if doesn't exists
        path = "./Datas/customers.json"
        isExist = os.path.exists(path)

        if isExist:
            pass
        else:
            # create JSON file
            # Data to be written
            json_file = {
                "customers": [

                ]
            }
            # Serializing json
            json_object = json.dumps(json_file, indent=4)
            # Writing file
            with open(path, "w") as outfile:
                outfile.write(json_object)

            # opening json file
            with open(path, 'rb') as file:
                data = json.load(file)
                temp = data['customers']

                # loading customers datas from csv
                csv_path = "./Datas/Registre Clients.csv"
                with open(csv_path, encoding="utf8") as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=';')

                    for row in spamreader:
                        if row[0] == 'ID':
                            continue
                        customer_id = row[0]
                        lastname = row[1]
                        firstname = row[2]
                        address = row[3]
                        zip_code = row[4]
                        city = row[5]
                        phone = row[6]
                        mobile = row[7]
                        mail = row[8]

                        # préparation des données de la catégorie
                        new_data = {
                            "id": customer_id,
                            "lastname": lastname,
                            "firstname": firstname,
                            "address": address,
                            "zip_code": zip_code,
                            "city": city,
                            "phone": phone,
                            "mobile": mobile,
                            "mail": mail
                        }

                        temp.append(new_data)

                        self.save_as_json(path, data)

    def load_books(self):
        # creating json file if doesn't exists
        path = "./Datas/books.json"
        isExist = os.path.exists(path)

        if isExist:
            pass
        else:
            # create JSON file
            # Data to be written
            json_file = {
                "books": [

                ]
            }
            # Serializing json
            json_object = json.dumps(json_file, indent=4)
            # Writing file
            with open(path, "w") as outfile:
                outfile.write(json_object)

            # opening json file
            with open(path, 'rb') as file:
                data = json.load(file)
                temp = data['books']

                # loading customers datas from csv
                csv_path = "./Datas/Registre Livres.csv"
                with open(csv_path, encoding="utf8") as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=';')

                    for row in spamreader:
                        if row[1] == 'Codebarre':
                            continue
                        book_id = row[0]
                        book_bar_code = row[1]
                        book_author = row[2]
                        book_name = row[3]
                        book_type = row[4]
                        book_price = row[5]
                        book_category = row[6]
                        book_abstract = row[7]
                        book_availability = True
                        book_list_borrowers = []
                        book_list_buyers = []

                        # préparation des données de la catégorie
                        new_data = {
                            "id": book_id,
                            "bar code": book_bar_code,
                            "author": book_author,
                            "name": book_name,
                            "type": book_type,
                            "price": book_price,
                            "category": book_category,
                            "abstract": book_abstract,
                            "availability": book_availability,
                            "list borrowers": book_list_borrowers,
                            "list_buyers": book_list_buyers
                        }

                        temp.append(new_data)

                        self.save_as_json(path, data)

    def load_employees(self):
        # creating json file if doesn't exists
        path = "./Datas/employees.json"
        isExist = os.path.exists(path)

        if isExist:
            pass
        else:
            # create JSON file
            # Data to be written
            json_file = {
                "employees": [

                ]
            }
            # Serializing json
            json_object = json.dumps(json_file, indent=4)
            # Writing file
            with open(path, "w") as outfile:
                outfile.write(json_object)

            # opening json file
            with open(path, 'rb') as file:
                data = json.load(file)
                temp = data['employees']

                # loading customers datas from csv
                csv_path = "./Datas/Registre Employes.csv"
                with open(csv_path, encoding="utf8") as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=';')

                    for row in spamreader:
                        if row[1] == 'Lastname':
                            continue
                        employee_id = row[0]
                        lastname = row[1]
                        firstname = row[2]
                        mail = row[3]
                        manager = row[4]
                        password = row[5]

                        # préparation des données de la catégorie
                        new_data = {
                            "id": employee_id,
                            "lastname": lastname,
                            "firstname": firstname,
                            "mail": mail,
                            "manager": manager,
                            "password": password
                        }

                        temp.append(new_data)

                        self.save_as_json(path, data)

    def borrows_history(self):
        path = "./Datas/borrows_history.json"
        isExist = os.path.exists(path)
        if isExist:
            pass
        else:
            json_file = {
                "borrows_history": [

                ]
            }
            json_object = json.dumps(json_file, indent=4)

            with open(path, "w") as outfile:
                outfile.write(json_object)

            with open(path, "rb") as file:
                data = json.load(file)
                temp = data["borrows_history"]

                csv_path = "./Datas/borrows_history.csv"
                with open(csv_path, encoding="utf-8") as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=";")

                    for row in spamreader:
                        if row[1] == "start_date":
                            continue
                        borrow_id = row[0]
                        start_date = row[1]
                        end_date = row[2]
                        list_book_id = row[3]
                        customer_id = row[4]

                        new_data = {
                            "id": borrow_id,
                            "start_date": start_date,
                            "end_date": end_date,
                            "list_book_id": list_book_id,
                            "customer_id": customer_id
                        }

                        temp.append(new_data)

                        self.save_as_json(path, data)


    def sales_history(self):
        path = "./Datas/sales_history.json"
        isExist = os.path.exists(path)
        if isExist:
            pass
        else:
            json_file = {
                "sales_history":[

                ]
            }
            json_object = json.dumps(json_file, indent=4)

            with open(path, "w") as outfile:
                outfile.write(json_object)

            with open(path, 'rb') as file:
                data = json.load(file)
                temp = data["sales_history"]

                csv_path = "./Datas/sales_history.csv"
                with open(csv_path, encoding="utf8") as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=";")

                    for row in spamreader:
                        if row[1] == "list_book_id":
                            continue
                        sale_id = row[0]
                        list_book_id = row[1]
                        amount = row[2]
                        customer_id = row[3]
                        transaction_date = row[4]

                        new_data = {
                            "id": sale_id,
                            "list_book_id": list_book_id,
                            "amount": amount,
                            "customer id": customer_id,
                            "transaction_date": transaction_date
                        }

                        temp.append(new_data)

                        self.save_as_json(path, data)


    def save_as_json(self, path, data):
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)
