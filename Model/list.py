from Model.Customer import Customer
from Model.Type import Type
from Model.book import Book
from Model.Employee import Employee
from Model.Category import Category
from Model.Sale import Sale
from Model.Borrow import Borrow
from Model.Author import Author
import os
import json
from datetime import datetime
import time


class ListObject:
    def __init__(self):
        self.list = []
        self.list_objects = []

    def load_list_of_books(self):
        self.list = []
        path = "./Datas/books.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                books_list = json_object['books']
                for book in books_list:
                    book_id = book['id']
                    book_barcode = book['bar code']
                    book_author = book['author']
                    book_name = book['name']
                    book_type = book['type']
                    book_price = book['price']
                    book_category = book['category']
                    book_abstract = book['abstract']
                    book_availability = book['availability']
                    book2 = Book(book_id, book_barcode, book_author, book_name,
                                book_type, book_price, book_category, book_abstract,
                                book_availability)
                    self.list.append(book2)
        return self.list


    def load_list_of_books_archive(self):
        self.list = []
        path = "./Datas/books_archive.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                books_list = json_object['books']
                for book in books_list:
                    book_id = book['id']
                    book_barcode = book['bar code']
                    book_author = book['author']
                    book_name = book['name']
                    book_type = book['type']
                    book_price = book['price']
                    book_category = book['category']
                    book_abstract = book['abstract']
                    book_availability = book['availability']
                    book2 = Book(book_id, book_barcode, book_author, book_name,
                                book_type, book_price, book_category, book_abstract,
                                book_availability)
                    self.list.append(book2)
        return self.list

    def load_list_of_author(self):
        list_author = []
        list_books = self.load_list_of_books()
        for book in list_books:
            if book.author in list_author:
                pass
            else:
                list_author.append(book.author)
        return list_author

    def load_list_of_type(self):
        self.list = []
        path = "./Datas/types.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                type_list = json_object['types']
                for item in type_list:
                    type_id = item['id']
                    type_label = item['label']
                    type = Type(type_id, type_label)
                    self.list.append(type)
        return self.list

    def load_list_of_categories(self):
        self.list = []
        path = "./Datas/categories.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                type_list = json_object['categories']
                for item in type_list:
                    categories_id = item['id']
                    categories_label = item['label']
                    categories = Category(categories_id, categories_label)
                    self.list.append(categories)
        return self.list


    def load_list_of_coustomers(self):
        self.list = []
        path = "./Datas/customers.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                customers_list = json_object['customers']
                for person in customers_list:
                    customer_id = person['id']
                    customer_lastname = person['lastname']
                    customer_firstname = person['firstname']
                    customer_address = person['address']
                    customer_zipcode = person['zip_code']
                    customer_city = person['city']
                    customer_phone = person['phone']
                    customer_mobile = person['mobile']
                    customer_mail = person['mail']

                    customer = Customer(customer_id, customer_lastname, customer_firstname, customer_address,
                                        customer_zipcode, customer_city, customer_phone, customer_mobile, customer_mail)
                    self.list.append(customer)
        return self.list

    def load_list_employees(self):
        self.list = []
        path = "./Datas/employees.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                employees_list = json_object['employees']

                for item in employees_list:
                    employee_id = item['id']
                    employee_lastname = item['lastname']
                    employee_firstname = item['firstname']
                    employee_mail = item['mail']
                    employee_manager = item['role']
                    employee_password = item['username']
                    employee = Employee(employee_id, employee_lastname, employee_firstname, employee_mail,
                                        employee_manager, employee_password)

                    self.list.append(employee)
        return self.list

    def load_borrows_list(self, book):
        customer_list = self.load_list_of_coustomers()
        borrow_history = []
        customers_id = []
        self.list = []
        path = "./Datas/borrows_history.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                employees_list = json_object['borrows_history']
                for item in employees_list:
                    borrow_id = item['id']
                    start_date = item['start_date']
                    end_date = item['end_date']
                    list_book_id = item['list_book_id']
                    customer_id = item['customer_id']
                    borrow = Borrow(list_books=list_book_id, start_date=start_date, end_date=end_date, client_id=customer_id)
                    borrow_history.append(borrow)
        for history in borrow_history:
            for books in history.list_book_id:
                if book.id == books:
                    customers_id.append(history.client_id)
        for customer in customer_list:
            if customer.id in customers_id:
                if customer not in self.list:
                    self.list.append(customer)
        return self.list

    def load_sales_list(self, book):

        customer_list = self.load_list_of_coustomers()
        sale_history = []
        customers_id = []
        self.list = []
        path = "./Datas/sales_history.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                employees_list = json_object['sales_history']
                for item in employees_list:
                    sale_id = item['id']
                    list_book_id = item['list_book_id']
                    amount = item['amount']
                    client_id = item['customer_id']
                    date = item['transaction_date']
                    sale = Sale(list_books=list_book_id,amount=amount, client_id=client_id, sales_date=date)
                    sale_history.append(sale)
        for history in sale_history:
            for list_book in history.list_book_id:
                if book.id == list_book:
                    customers_id.append(history.client_id)
        for customer in customer_list:
            if customer.id in customers_id:
                if customer not in self.list:
                    self.list.append(customer)

        return self.list

    def nbr_borrows(self, book):
        borrow_history = []
        i = 0
        path = "./Datas/borrows_history.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                list_borrow = json_object['borrows_history']
                for borrow in list_borrow:
                    for id in borrow['list_book_id']:
                        if id == book.id:
                            i+=1
                            borrow_history.append(borrow)
        return borrow_history

    def nbr_sales(self, book):
        sale_history = []
        i = 0
        path = "./Datas/sales_history.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open(path, 'rb') as file:
                json_object = json.load(file)
                list_sale = json_object['sales_history']
                for sale in list_sale:
                    for id in sale['list_book_id']:
                        if id == book.id:
                            i+=1
                            sale_history.append(sale)
        return sale_history

    def nbr_sales_by_author(self, author):
        sale_history = []
        list_book_author = []
        i = 0
        path ="./Datas/Books.json"
        path_history = "./Datas/sales_history.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open (path, 'rb') as file:
                json_object = json.load(file)
                list_book = json_object['books']
                for book in list_book:
                    if book['author'] == author:
                        list_book_author.append(book)

            for book in list_book_author:
                with open(path_history, 'rb') as file:
                    json_object = json.load(file)
                    list_sale = json_object['sales_history']
                    for sale in list_sale:
                        for id in sale['list_book_id']:
                            if id == book['id']:
                                sale_history.append(sale)
        return sale_history

    def nbr_sales_by_type(self, type):
        sale_history = []
        list_book_type = []
        i=0
        path = "./Datas/Books.json"
        path_history = "./Datas/sales_history.json"
        is_exist = os.path.exists(path)
        if is_exist:
            with open (path, 'rb') as file:
                json_object = json.load(file)
                list_book = json_object['books']
                for book in list_book:
                    if book['type'] == type:
                        list_book_type.append(book)

            for book in list_book_type:
                with open(path_history, 'rb') as file:
                    json_object = json.load(file)
                    list_sale = json_object['sales_history']
                    for sale in list_sale:
                        for id in sale['list_book_id']:
                            if id == book['id']:
                                sale_history.append(sale)
        return sale_history

    def load_list_customer_transactions(self, transaction_type, customer_id):
        # path
        folder_name = transaction_type + "_history"
        path = "./Datas/" + folder_name + ".json"
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_transactions = data[folder_name]
            list_customer_transactions = []
            list_books = []

            for transaction in list_transactions:
                if transaction['customer_id'] == customer_id:
                    for book in transaction["list_book_id"]:
                        list_customer_transactions.append(book)
                        book = self.generate_book_object(book)
                        list_books.append(book)

            return list_books

    def generate_book_object(self, book_id):
        # path
        path = './Datas/books.json'
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_books = data['books']

            for item in list_books:
                if item["id"] == book_id:
                    book = Book(item['id'], item['bar code'], item['author'], item['name'], item['type'], item['price'],
                                item['category'], item['abstract'], item['availability'])

                    return book

    def list_borrows(self, customer_id):
        # date du jour
        now_str = datetime.today().strftime('%d/%m/%Y')
        now_datetime = datetime.strptime(now_str, "%d/%m/%Y")
        customer_borrows = self.get_customer_borrows(customer_id)
        alerts_list = []

        for borrow in customer_borrows:
            end_date_str = borrow.end_date
            end_date_datetime = datetime.strptime(end_date_str, "%d/%m/%Y")

            if end_date_datetime <= now_datetime:
                borrow_object = Borrow(borrow.id, borrow.list_book_id, borrow.start_date,
                                       borrow.end_date, borrow.client_id)
                borrow_object.list_books = self.get_customer_late_books(borrow_object.list_book_id)
                alerts_list.append(borrow_object)

            print(now_str)
            print(end_date_str)
            print(now_datetime)
            print(end_date_datetime)

        print(alerts_list)

        return alerts_list

    def get_customer_borrows(self, customer_id):
        customer_borrows = []
        # path
        path = './Datas/borrows_history.json'
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_borrows = data['borrows_history']

            for borrow in list_borrows:
                if borrow['customer_id'] == customer_id:
                    borrow_object = Borrow(borrow['id'], borrow['list_book_id'], borrow['start_date'],
                                           borrow['end_date'], borrow['customer_id'])
                    borrow_object.list_books = self.get_customer_late_books(borrow_object.list_book_id)
                    customer_borrows.append(borrow_object)

            return customer_borrows

    def get_customer_boughts(self, customer_id):
        customer_sales = []
        # path
        path = './Datas/sales_history.json'
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_borrows = data['sales_history']

            for borrow in list_borrows:
                if borrow['customer_id'] == customer_id:
                    customer_sales.append(borrow)

        return customer_sales

    def get_customers_favorites(self, customer_id):
        list_object = ListObject()
        list_borrows = list_object.get_customer_borrows(customer_id)
        list_boughts = list_object.get_customer_boughts(customer_id)
        list_customer_books = self.get_customer_books(list_borrows, list_boughts)
        customer_authors = self.get_customer_authors(list_customer_books)
        customer_categories = self.get_customer_categories(list_customer_books)
        customer_types = self.get_customer_types(list_customer_books)
        customer_favorite_author = self.get_favorite_author(list_customer_books, customer_authors)
        customer_favorite_category = self.get_favorite_category(list_customer_books, customer_categories)
        customer_favorite_type = self.get_favorite_type(list_customer_books, customer_types)

        list_customer_favorites = {
            "author": customer_favorite_author,
            "category": customer_favorite_category,
            "type": customer_favorite_type
        }

        return list_customer_favorites

    def get_customer_authors(self, list_customer_books):
        customer_authors = []
        author_names = []
        for book in list_customer_books:
            author = Author(book.author)
            if book.author in author_names:
                pass
            else:
                author_names.append(book.author)
                customer_authors.append(author)

        return customer_authors

    def get_favorite_author(self, list_borrowed_books, customer_authors):
        for author in customer_authors:
            for book in list_borrowed_books:
                if book.author == author.name:
                    author.favorites_count += 1

        customer_authors.sort(key=lambda author: author.favorites_count, reverse=True)

        if customer_authors:
            customer_favorite_author = customer_authors[0].name
        else:
            customer_favorite_author = ""

        return customer_favorite_author

    def get_favorite_category(self, list_borrowed_books, customer_categories):
        for category in customer_categories:
            for book in list_borrowed_books:
                if book.category == category.label:
                    category.favorites_count += 1

        customer_categories.sort(key=lambda category: category.favorites_count, reverse=True)

        if customer_categories:
            customer_favorite_category = customer_categories[0].label
        else:
            customer_favorite_category = ""

        return customer_favorite_category

    def get_favorite_type(self, list_borrowed_books, customer_types):
        for customer_type in customer_types:
            for book in list_borrowed_books:
                if book.type == customer_type.type:
                    customer_type.favorites_count += 1

        customer_types.sort(key=lambda customer_type: customer_type.favorites_count, reverse=True)

        if customer_types:
            customer_favorite_customer_type = customer_types[0].type
        else:
            customer_favorite_customer_type = ""

        return customer_favorite_customer_type

    def get_customer_categories(self, list_borrowed_books):
        list_categories = ListObject.load_list_of_categories(self)
        customer_categories = []
        categories_names = []
        for book in list_borrowed_books:
            for category in list_categories:
                if category.label == book.category:
                    category_object = Category(category.id, book.category)
                    if book.category in categories_names:
                        pass
                    else:
                        categories_names.append(book.category)
                        customer_categories.append(category_object)

        return customer_categories

    def get_customer_types(self, list_borrowed_books):
        list_types = ListObject.load_list_of_type(self)
        customer_types = []
        types_names = []
        for book in list_borrowed_books:
            for type in list_types:
                if type.type == book.type:
                    type_object = Type(type.id, book.type)
                    if book.type in types_names:
                        pass
                    else:
                        types_names.append(book.type)
                        customer_types.append(type_object)

        return customer_types

    def get_customer_books(self, list_borrows, list_boughts):
        list_customer_books = []

        for borrow in list_borrows:
            for book_id in borrow.list_book_id:
                list1 = ListObject()
                list_books = list1.load_list_of_books()
                for book in list_books:
                    if book_id == book.id:
                        list_customer_books.append(book)

        for sale in list_boughts:
            for book_id in sale['list_book_id']:
                list1 = ListObject()
                list_books = list1.load_list_of_books()
                for book in list_books:
                    if book_id == book.id:
                        list_customer_books.append(book)

        return list_customer_books

    def get_customer_late_books(self, borrow_books_ids):
        list_late_books = []
        list1 = ListObject()
        list_books = list1.load_list_of_books()

        for book_id in borrow_books_ids:
            for book in list_books:
                if book_id == book.id:
                    list_late_books.append(book)

        return list_late_books

    def save_as_json(self, path, data):
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)

    def set_customer_id(self, path):
        file_object = open(path, "r")
        json_content = file_object.read()
        users_list = json.loads(json_content)
        nb_users = len(users_list['customers'])
        user_id = str(nb_users + 1)
        file_object.close()

        return user_id
