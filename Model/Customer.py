from Model.person import Person
from Model.book import Book
import json


class Customer(Person):
    def __init__(self, customer_id, lastname, firstname, mail, customer_address, customer_zipcode, customer_city,
                 customer_phone, customer_mobile):
        Person.__init__(self, lastname, firstname, mail)
        self.id = customer_id
        self.lastname = lastname
        self.firstname = firstname
        self.address = customer_address
        self.zipcode = customer_zipcode
        self.city = customer_city
        self.phone = customer_phone
        self.mobile = customer_mobile
        self.mail = mail

    def save_customers(self, path, customer_id, lastname, firstname, address, zip_code, city, phone, mobile, mail):
        # opening json file
        path = path
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['customers']
            # get new_customer datas from add customer form
            customer_id = customer_id
            # préparation des données de la catégorie
            new_data = {
                "id": customer_id,
                "lastname": lastname,
                "firstname": firstname,
                "address": address,
                "zip_code": zip_code,
                "city": city,
                "phone": phone,
                "mobile": mobile,
                "mail": mail
            }
            temp.append(new_data)
        return data

    def set_customer_id(self, path):
        file_object = open(path, "r")
        json_content = file_object.read()
        users_list = json.loads(json_content)
        nb_users = len(users_list['customers'])
        user_id = str(nb_users + 1)
        file_object.close()
        return user_id

    def delete_customer(self, path, customer_id):
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_customer = data['customers']
            temp = []

            for item in list_customer:
                if item['id'] != customer_id:
                    temp.append(item)
        # json_object
        json_object = {
                "customers": temp
            }

        return json_object

    def transactions_history(self, transaction_type):
        if transaction_type == "sales":
            key = "customer id"
        else:
            key = "customer_id"

        # path
        folder_name = transaction_type + "_history"
        path = "./Datas/" + folder_name + ".json"
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_transactions = data[transaction_type]
            list_customer_transactions = []
            list_books = []

            for transaction in list_transactions:
                if transaction[key] == self.id:
                    for book_id in transaction["list_book_id"]:
                        book = Customer.generate_book_object(self, book_id)
                    '''list_customer_transactions.append(transaction["list_book_id"])

            for book_id in list_customer_transactions:
                book = Customer.generate_book_object(self, book_id)
                list_books.append(book)'''

            return list_books

    def generate_book_object(self, book_id):
        books = []
        books_list = []
        # path
        path = './Datas/books.json'
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_books = data['books']

            for item in list_books:
                if item["id"] == book_id:
                    books.append(item["id"])

        '''for item in book_id:
            for book in books:
                book = Book(item["id"], item["bar code"], item["author"], item["name"], item["type"], item["price"],
                            item["category"], item["abstract"], item["availability"], item["list borrowers"],
                            item["list_buyers"])

                books_list.append(book)'''

        return books_list

    def borrows_history(self, customer_id):
        pass